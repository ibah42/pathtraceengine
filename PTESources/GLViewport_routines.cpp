#include "PTESources/PCH.hpp"
#include "PTESources/GLViewport_routines.hpp"
#include "PTESources/RenderSystem/BVH.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

PTE::Scene * gScene = 0;
PTE::Camera * gCamera= 0;
PTE::GL::Timer gTimer;

static float deltaTime = 100;
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{
namespace GL{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
	

SceneSettings::SceneSettings()
{
	FOV=90.f;
	near_clip=0.001f;
	far_clip = 9E+10f;
	restInterestDistance = 10.1f; 

	zoom_step = 3.0f;
	orbit_step = 60.f;
	pan_step = 10.f;

#ifdef _DEBUG
	winSize_h = 100;
	winSize_w = 100;
#else
	winSize_h = 600;
	winSize_w = 600;
#endif
	cam_pos = glm::vec3(0.0, 2.0, restInterestDistance); 
	cam_interest = glm::vec3(0.0, 0.0, 0.0) ;
	cam_up = glm::vec3(0.0, 1.0, 0.0 );
	cam_restPos = glm::vec3 (0.0, 0.0, 0.0);
	
	drawHomeGrid = true;	
	drawAllAsWire = true;
	drawTextures = true;
	drawPathtraced = false;

	drawTrigs = true;
	drawEmptyAABBs = false;
	maxDepthDraw = 10000;
	drawTree = false;

} // SceneSettings::SceneSettings


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
SceneSettings::defaultPrefsLookAt()
{
	gluLookAt ( 
			cam_pos.x
		,	cam_pos.y
		,	cam_pos.z   // campos ( eye )
	
		,	cam_interest.x
		,	cam_interest.y
		,	cam_interest.z // cam interest pos ( center )
		
		,	cam_up.x
		,	cam_up.y
		,	cam_up.z // up vector
	);
} // SceneSettings::defaultPrefsLookAt


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


glm::mat3
SceneSettings::Get3x3RotMatrix(const float degrees, const glm::vec3& up) const 
{
	float rad = degrees * Pi / 180.f;
		
	// You will change this return call
	return glm::mat3(
			cosf( rad ) + up.x*up.x * ( 1.f-cosf(rad) )
		,	up.x*up.y * (1.f-cosf(rad)) - up.z * sinf(rad)
		,	up.x * up.z*(1.f-cosf(rad)) + up.y * sinf(rad)

		,	up.y*up.x* ( 1.f-cosf(rad))  + up.z*sinf(rad)
		,	cosf(rad) + up.y * up.y*( 1.f - cosf(rad) )
		,   up.z*up.y*( 1.f-cosf(rad) ) - up.x*sinf(rad)

		,	up.z*up.x*(1.f-cosf(rad))-up.y*sinf(rad)
		,	up.z*up.y*(1.f-cosf(rad))+up.x*sinf(rad)
		,	cosf(rad) + up.z*up.z*(1.f-cosf(rad))
	);

} // SceneSettings::Get3x3RotMatrix


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
DisplayPrimitives::DrawSphere ( GLfloat in_size )
{
	glLineWidth(1.f);
	glColor3f(0.f,0.f,0.f);
	if ( gPrefs.drawAllAsWire==false ) 
	{ 
		glColor3f(1.f,1.f,1.f);
		glutSolidSphere ( in_size, 50, 50 );
		glColor3f(0.f,0.f,0.f);
		if ( gPrefs.drawWireOnShaded )
			glutWireSphere ( in_size, 50, 50 );
	}
	else
	
	glutWireSphere ( in_size, 50, 50 );
	glutPostRedisplay ();

} // DisplayPrimitives::DrawSphere


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
DisplayPrimitives::DrawTeapot (  GLfloat in_size )
{
	glLineWidth(1.f);
	glColor3f(0.f,0.f,0.f);
	if ( gPrefs.drawAllAsWire==false ) 
	{ 
		glColor3f(1.f,1.f,1.f);
		glutSolidTeapot ( in_size );
		glColor3f(0.f,0.f,0.f);
		if ( gPrefs.drawWireOnShaded )
			glutWireTeapot ( in_size );
	}
	else
		glutWireTeapot ( in_size ); 

	glutPostRedisplay ();

} // DisplayPrimitives::DrawTeapot


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
DisplayPrimitives::DrawHomeGrid ( GLfloat in_length )
{
	glDisable ( GL_LIGHTING ) ;
	glLineWidth(1.f);
	glColor3f(0.3f,0.3f,0.3f);
	glBegin(GL_LINES);
	for ( GLfloat i=0;i<20.1f; ++ i) // 21 = number of lines
	{
		if ( cmpEqual( i, 10.0f, 0.001f ) )
		{
			glColor3f(0.f,0.f,0.f); 
			glVertex3f( 0.f, 0.f, linearInterpolation(-in_length,in_length,i/20.f));
			glVertex3f( -in_length, 0.f, linearInterpolation(-in_length,in_length,i/20.f));
			glColor3f(0.3f,0.3f,0.3f); 
		}
		else
		{
			glVertex3f( in_length, 0.f, linearInterpolation(-in_length,in_length,i/20.f));
			glVertex3f( -in_length, 0.f, linearInterpolation(-in_length,in_length,i/20.f));
		}
	}
	for ( GLfloat i=0;i<20.1; ++ i) // 21 = number of lines
	{
		if ( cmpEqual( i, 10.0f, 0.001f ) )
		{
			glColor3f(0.f,0.f,0.f); 
			glVertex3f (linearInterpolation(-in_length,in_length,i/20.f), 0.f, 0.f);
			glVertex3f( linearInterpolation(-in_length,in_length,i/20.f), 0.f,-in_length);			
			glColor3f(0.3f,0.3f,0.3f);
		}
		else
		{
			glVertex3f( linearInterpolation(-in_length,in_length,i/20.f), 0.f,in_length);
			glVertex3f( linearInterpolation(-in_length,in_length,i/20.f), 0.f,-in_length);	
		}
	}

	glEnd();

	// draw axes
	glLineWidth(2.f);
	glBegin(GL_LINES);
	glColor3f(1.f,0.f,0.f);
	glVertex3f(0.f,0.f,0.f); glVertex3f(in_length,0.f,0.f);

	glColor3f(0.f,1.f,0.f);
	glVertex3f(0.f,0.f,0.f); glVertex3f(0.f,in_length,0.f);

	glColor3f(0.f,0.f,1.f);
	glVertex3f(0.f,0.f,0.f); glVertex3f(0.f,0.f,in_length);

	glEnd();
	glEnable(GL_LIGHTING);
	glutPostRedisplay ();	

} // DisplayPrimitives::DrawHomeGrid


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
DisplayPrimitives::DrawPoints ( GLfloat in_size )
{
	glDisable ( GL_LIGHTING );

	glPointSize ( in_size );
	glBegin(GL_POINTS);
	glColor4f ( 1.f, 0.f, 0.f, 0.5f );
	for ( int i=0;i < 1000; ++i )
	{
		glVertex3f ( float(i % 10), float( (i / 10 ) % 10 ),  i / 100.f );
	}

	glVertex3f ( gPrefs.cam_interest.x,		gPrefs.cam_interest.y,		gPrefs.cam_interest.z );
	glVertex3f ( gPrefs.cam_interest.x+1,	gPrefs.cam_interest.y,		gPrefs.cam_interest.z );
	glVertex3f ( gPrefs.cam_interest.x,		gPrefs.cam_interest.y+1,	gPrefs.cam_interest.z );
	glVertex3f ( gPrefs.cam_interest.x,		gPrefs.cam_interest.y,		gPrefs.cam_interest.z+1 );

	glEnd();

	glEnable ( GL_LIGHTING );

	glutPostRedisplay ();	

} // DisplayPrimitives::DrawPoints


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void 
DisplayPrimitives::DrawDebugTriangles  ( PTE::Camera & in_camera )
{
	/*
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	void* data = gCamera->getRGBAas8888();
	glDrawPixels( gCamera->m_nbPixelsX, gCamera->m_nbPixelsY, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, data );
	free( data );
	*/

    glDisable ( GL_LIGHTING );
    glPointSize ( 1.f );
    glBegin(GL_POINTS);

//     // get framebuffer size
//     int xRes = in_camera.m_nbPixelsX;
//     int yRes = in_camera.m_nbPixelsY;
//     int xyRes = xRes*yRes;
//     float posScl = 0.02f; // position scale
// 
//     // get framebuffer content
//     PTE::Color * pColorBuffStorage = in_camera.m_screenBuffer;
//         
//     // draw projection matrix
//     for ( int i=0; i<xyRes; ++i )
//     {
//         glColor4f (
// 				pColorBuffStorage->r
// 			,	pColorBuffStorage->g
// 			,	pColorBuffStorage->b
// 			,	1//pColorBuffStorage->alpha
//         );
//         glVertex3f (
// 				float( i % xRes ) * posScl
// 			,	float( ( i % xyRes ) / xRes ) * posScl
// 			,	0.f
//         );
//             
//         ++ pColorBuffStorage;
//     }

    // finish GL
    glEnd();
	
	glColor4f( 1,1,1,1 );
    // draw geometrical triangles
    glBegin(GL_TRIANGLES);

    for ( int i=0; i<gScene->getTrianglesCount(); ++i )
    {
		const PTE::Triangle & triRef = gScene->getTriangle(i);
		glVertex3f(triRef.p0.x, triRef.p0.y, triRef.p0.z);  
		glVertex3f(triRef.p1.x, triRef.p1.y, triRef.p1.z);   
		glVertex3f(triRef.p2.x, triRef.p2.y, triRef.p2.z);    
    };

    glEnd();

    /*
    // draw camera rays
    glLineWidth(2.f);
    glBegin(GL_LINES);
    glColor3f(1.f,1.f,1.f);


    for ( int i=0; i<xyRes;  i += 32 )
    {
            PTE::Ray & rayRef = in_camera.getRay(
                            i % in_camera.m_nbPixelsX
                    ,       ( i % xyRes ) / in_camera.m_nbPixelsX 
            );

            glVertex3f(rayRef.origin.x, rayRef.origin.y, rayRef.origin.z);
            glVertex3f(rayRef.dir.x, rayRef.dir.y, rayRef.dir.z);
    };

    glEnd();
    */

    glEnable ( GL_LIGHTING );
    glutPostRedisplay (); 
	
}; // DisplayPrimitives::DrawDebugTriangles 


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void renderLine( const Vec3& v1, const Vec3& v2, const Color& c )
{
	glBegin(GL_LINES);

	glColor3f (c.r, c.g, c.b );
	glVertex3f( v1.x, v1.y, v1.z );
	glVertex3f( v2.x, v2.y, v2.z );
		
	glEnd();	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void DrawAABB( const AABB& box, int depth, int trigsIn )
{
	if ( gPrefs.maxDepthDraw != depth && gPrefs.maxDepthDraw < 100 )
		return;

	Color c;

	switch( trigsIn )
	{

	case 0:	c.set( 1.0f, 1.0f, 1.0f ); break;
	case 1:	c.set( 1.0f, 0.9f, 0.9f ); break;
	case 2:	c.set( 1.0f, 0.8f, 0.8f ); break;
	case 3:	c.set( 1.0f, 0.7f, 0.7f ); break;
	case 4:	c.set( 1.0f, 0.6f, 0.6f ); break;
	case 5:	c.set( 1.0f, 0.5f, 0.5f ); break;
	case 6:	c.set( 1.0f, 0.4f, 0.4f ); break;
	default:c.set( 1.0f, 0, 0 ); break;

	}

	if ( !gPrefs.drawEmptyAABBs)
		if ( trigsIn == 0 || trigsIn == 1 )
			return;
	
	Vec3 v1,v2,v3,v4,v5,v6,v7,v8;

	glLineWidth( 1.0 );
	v4 = box.min();
	v6 = box.max();
	
	v1.set( v4.x, v4.y, v6.z );
	v2.set( v6.x, v4.y, v6.z );
	v3.set( v6.x, v4.y, v4.z );

	v5.set( v1.x, v6.y, v1.z );
	v7.set( v3.x, v6.y, v3.z );
	v8.set( v4.x, v6.y, v4.z );
	
		
	renderLine( v1, v2, c );
	renderLine( v2, v3, c );
	renderLine( v3, v4, c );
	renderLine( v4, v1, c );


	renderLine( v1, v5, c );
	renderLine( v2, v6, c );
	renderLine( v3, v7, c );
	renderLine( v4, v8, c );
	
	renderLine( v5, v6, c );
	renderLine( v6, v7, c );
	renderLine( v7, v8, c );
	renderLine( v8, v5, c );	
	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void DisplayPrimitives::DrawDebugBVH ( BVHNode* node, int depth )
{
	glDisable ( GL_LIGHTING );
	depth++;

	DrawAABB( node->mBounds, depth, node->mTrigs.mCount );

	if ( node->mLeft )
		DrawDebugBVH( node->mLeft, depth );

	if ( node->mRight )
		DrawDebugBVH( node->mRight, depth );

	glEnable ( GL_LIGHTING );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Callbacks::ShowFPS ( float _dt )
{	
	glDisable ( GL_LIGHTING );
	gPrefs.lastFrameEvalTime = _dt; 
	glColor3f ( 1.f,1.f,1.f );

	// set orto
	Callbacks::SetOrthographicProjection ();

	// calc and show
	std::string strFps( " FPS" );
	float val = clamp ( -99999.9f, 99999.9f, 1.f / gPrefs.lastFrameEvalTime );
	char sbuff[500];
	sprintf_s ( sbuff, "%4.1f", val );
	glPushMatrix();
	glLoadIdentity();
	glRasterPos3f( gPrefs.winSize_w/2 - 30.f ,gPrefs.winSize_h - 10.0f, 0.0f );
	
	for ( int i=0;i<4;i++ )
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15 , sbuff[i]);
	// add info
	
	for ( int i=0;i<strFps.size(); ++ i )
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, strFps[i] );
	
	glPopMatrix();

	// restore perspective
	Callbacks::SetPerspectiveProjection ();
	glEnable ( GL_LIGHTING );
	glutPostRedisplay ();

	std::string str( "PTE, PT-mode, FPS : " );
	str += sbuff;
	str += " Current Adaptive AAx";
	sprintf_s ( sbuff, "%d", gScene->mAACount );
	str += sbuff;

	if ( _dt > 2.f )
		std::cout<<"Render-image finished at "<<_dt<<" seconds\n";

	glutSetWindowTitle( str.c_str() );

} // Callbacks::ShowFPS


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


/*
void
Callbacks::SetLight ( )
{
		// set up light colors (ambient, diffuse, specular)
	GLfloat lightKa[] = {.2f, .2f, .2f, 1.0f}  // ambient light
	GLfloat lightKd[] = {.7f, .7f, .7f, 1.0f}  // diffuse light
	GLfloat lightKs[] = {1, 1, 1, 1}           // specular light
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightKa);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightKd);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightKs);

	// position the light
	float lightPos[4] = {gPrefs.light_pos.x, gPrefs.light_pos.y, gPrefs.light_pos.z, gPrefs.lightIsOmni} // positional light
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glEnable(GL_LIGHT0);                        // MUST enable each light source after configuration
}
*/


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Callbacks::SetOrthographicProjection()
{
	// switch to projection mode
	glMatrixMode(GL_PROJECTION);

	// save previous matrix which contains the
	//settings for the perspective projection
	glPushMatrix();

	// reset matrix
	glLoadIdentity();

	// set a 2D orthographic projection
	gluOrtho2D(0, gPrefs.winSize_w,  gPrefs.winSize_h, 0);

	// switch back to modelview mode
	glMatrixMode(GL_MODELVIEW);

} // Callbacks::SetOrthographicProjection


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

void
Callbacks::SetPerspectiveProjection()
{
	glMatrixMode(GL_PROJECTION);
	// restore previous projection matrix
	glPopMatrix();

	// get back to modelview mode
	glMatrixMode(GL_MODELVIEW);

} // Callbacks::SetPerspectiveProjection


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*


void
Callbacks::DisplayLabel ( float in_val, std::string & in_info, float xpos, float ypos )
{
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Callbacks::SetLight()
{
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Callbacks::PrintHelp ()
{
	printf(
			"%s"

		,	"\npress 'h' to print help.			\n"	
			"press '+' or '-' to change FOV		\n"
			"press 'z' to center camera SRT		\n"
			"press 'g' to show / hide home grid \n"     
			"press arrows to move light and v to switch light type omni/directed \n"
			"press ESC to quit.		\n"
	);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Callbacks::InitFrame ()
{
	// print help once
	PrintHelp ();

	glShadeModel(GL_FLAT);                    // shading method: GL_SMOOTH or GL_FLAT
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);      // 4-byte pixel alignment

	// enable /disable features
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	//glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	//glEnable(GL_DEPTH_TEST);
	//glEnable(GL_LIGHTING);
	//glEnable(GL_TEXTURE_2D);
	//glEnable(GL_CULL_FACE);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable( GL_BLEND );

	// track material ambient and diffuse from surface color, call it before glEnable(GL_COLOR_MATERIAL)
	//glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	//glEnable(GL_COLOR_MATERIAL);

	glClearColor( 0.5f, 0.5f, 0.5f, 0.5f );      // background color
	glClearStencil(0);                          // clear stencil buffer

	gTimer.reset();
} // Callbacks::InitFrame


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Callbacks::DisplayScene() 
{
	gTimer.reset();

	glClear( GL_COLOR_BUFFER_BIT |  GL_DEPTH_BUFFER_BIT |  GL_STENCIL_BUFFER_BIT );
	glShadeModel(GL_SMOOTH); 
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity ();

	gPrefs.defaultPrefsLookAt();

	// process lights
	//SetLight  ();


	Assert(gScene && gCamera);
		
	gCamera->setParams( gPrefs.winSize_w, gPrefs.winSize_h, 1 );
	gCamera->updateCameraAttributes(
			( Vec3 )gPrefs.cam_pos
		,	( Vec3 )gPrefs.cam_up
		,	( ( Vec3 )gPrefs.cam_interest - ( Vec3 )gPrefs.cam_pos ).getNormalized()
		,	gPrefs.FOV
	);


	if ( gPrefs.drawPathtraced )
	{
		gScene->update( *gCamera, deltaTime );

		Callbacks::SetOrthographicProjection ();
		glPushMatrix();
		glLoadIdentity();
		glRasterPos3f( 0.0f, float(gPrefs.winSize_h), 0.0f );

		glDrawPixels(
				gCamera->mNbPixelsX
			,	gCamera->mNbPixelsY
			,	GL_RGB
			,	GL_FLOAT
			,	gCamera->getPrimeRenderBuffer().mColorBuff
		);

		// restore perspective
		glPopMatrix();
		Callbacks::SetPerspectiveProjection ();

		glutPostRedisplay();
	}
	else
	{
		if ( gPrefs.drawHomeGrid )
			DisplayPrimitives::DrawHomeGrid ( 200.0 );
		
		if ( gPrefs.drawTrigs )
			DisplayPrimitives::DrawDebugTriangles ( *gCamera );

		DisplayPrimitives::DrawDebugBVH( gScene->getTree(), 0 );
	}

	float dt = gTimer.getElapsedTime();
	deltaTime = dt;
	ShowFPS ( dt );
	glutSwapBuffers();

} // Callbacks::DisplayScene


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Callbacks::SpecialKeys(int key, int x, int y)
{
	
	if(key == GLUT_KEY_UP)
	{
		
	}
	if(key == GLUT_KEY_DOWN)
	{
		
	}
	if(key == GLUT_KEY_LEFT)
	{
			
	}
	if(key == GLUT_KEY_RIGHT)
	{
			
	}
	if (key == GLUT_KEY_F4 )
	{
		gPrefs.drawPathtraced = !gPrefs.drawPathtraced;
		printf( "%s", "Toggled Pathtrace/GL rendering\n" );
	}
	if (key == GLUT_KEY_F3 )
	{
		gPrefs.drawEmptyAABBs = ! gPrefs.drawEmptyAABBs;
	}
	if (key == GLUT_KEY_F6 )
	{
		gPrefs.drawTrigs = ! gPrefs.drawTrigs;
	}

	if (key == GLUT_KEY_F8 )
	{
		gPrefs.drawTree = ! gPrefs.drawTree;
	}
	if (key == GLUT_KEY_F5 )
	{
		gScene->invalidateCachedPositions();
		printf( "%s", "Toggled pixel sampling x" );
		switch( gScene->mAACount )
		{
			case 1:
				gScene->mAACount = 4; printf( "%s", "4\n" ); break;

			case 4:
				gScene->mAACount = 16; printf( "%s", "16\n" );  break;

			case 16:
				gScene->mAACount = 64; printf( "%s", "64\n" );  break;
			
			case 64:
				gScene->mAACount = 1; printf( "%s", "1\n" );  break;
		}
	}

	// redraw
	glutPostRedisplay();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Callbacks::MouseClickEvent ( int button, int state, int x, int y )
{
	gInputState.m_isModKeyPressed = glutGetModifiers() == GLUT_ACTIVE_ALT;
	gInputState.m_button = button;
	gInputState.m_state = state;
	gInputState.m_clickX = x;
	gInputState.m_clickY = y;
	gInputState.m_clickCameraPos = gPrefs.cam_pos;
	gInputState.m_clickCameraInterest = gPrefs.cam_interest;
	gInputState.m_clickCameraUp = gPrefs.cam_up;
	GLfloat dist = 0;

	switch ( button )
	{
	case GLUT_LEFT_BUTTON:  
		// check if we have mod key, if no - lets select object under the mouse
			break;
	case GLUT_RIGHT_BUTTON:
		break;

	case GLUT_MIDDLE_BUTTON:
		break;

	case 3: // wheel up
		dist = glm::distance ( gPrefs.cam_pos, gPrefs.cam_interest );
		if ( cmpEqual(0.f,dist,gPrefs.near_clip*1.1f) )
			break;

		if ( dist > 5.f * gPrefs.zoom_step )
		{
			glm::vec3 zoom = glm::normalize (  gPrefs.cam_pos - gPrefs.cam_interest );
			zoom *= gPrefs.zoom_step + dist*0.01f;
			gPrefs.cam_pos -= zoom;
		}
		else
		{
			glm::vec3 zoom = glm::normalize (  gPrefs.cam_pos - gPrefs.cam_interest );
			zoom *= dist / 5.f * gPrefs.zoom_step;
			gPrefs.cam_pos -= zoom;
		}

		break;

	case 4: // wheel down
		dist = glm::distance ( gPrefs.cam_pos, gPrefs.cam_interest );
		if ( dist > 5.f * gPrefs.zoom_step )
		{
			glm::vec3 zoom = glm::normalize (  gPrefs.cam_pos - gPrefs.cam_interest );
			zoom *= gPrefs.zoom_step + dist*0.01f;
			gPrefs.cam_pos += zoom;
		}
		else
		{
			glm::vec3 zoom = glm::normalize (  gPrefs.cam_pos - gPrefs.cam_interest );
			zoom *= dist / 5.f * gPrefs.zoom_step;
			gPrefs.cam_pos += zoom;
		}

		break;
	}

	// upd camera
	glMatrixMode(GL_MODELVIEW) ;
	glLoadIdentity() ;
	gPrefs.defaultPrefsLookAt();
	glutPostRedisplay();

} // Callbacks::MouseClickEvent 


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Callbacks::MouseDragEvent ( int x, int y )
{
	if ( gInputState.m_isModKeyPressed )
	{
		switch (  gInputState.m_button )
		{
			case GLUT_LEFT_BUTTON:  // rotate camera ( orbit )
			{
				float angle_w = ((float)(gInputState.m_clickX - x))/100.f; //((float)gPrefs.winSize_w);
				float angle_h = ((float)(gInputState.m_clickY - y))/100.f; //((float)gPrefs.winSize_h);
	
				// reset up vector to its state on mouse click
				gPrefs.cam_up = gInputState.m_clickCameraUp;
				gInputState.m_clickCameraPos -= gInputState.m_clickCameraInterest;

				glm::vec3 rotAxis = glm::normalize ( glm::cross ( gInputState.m_clickCameraUp, gInputState.m_clickCameraPos) );						
				gPrefs.cam_pos =  (gInputState.m_clickCameraPos ) * gPrefs.Get3x3RotMatrix ( angle_h * gPrefs.orbit_step, rotAxis );
									
				if (  glm::dot (rotAxis , glm::normalize ( glm::cross ( gInputState.m_clickCameraUp, gPrefs.cam_pos) )) < -0.01f )
				{
					gPrefs.cam_up = -gInputState.m_clickCameraUp;
				}

				// horizontal rot
				gPrefs.cam_pos = gPrefs.cam_pos * gPrefs.Get3x3RotMatrix ( angle_w * gPrefs.orbit_step, gInputState.m_clickCameraUp );	
				gInputState.m_clickCameraPos += gInputState.m_clickCameraInterest;
				gPrefs.cam_pos += gInputState.m_clickCameraInterest;
			
				break;
			}
			
			
			case GLUT_RIGHT_BUTTON: // soft zoom 
			{
				break;		
			}
			
			case GLUT_MIDDLE_BUTTON: // translate camera ( pan )
			{
				float offset_w = ((float)(gInputState.m_clickX - x))/100.f;//((float)gPrefs.winSize_w);
				float offset_h = ((float)(gInputState.m_clickY - y))/100.f;//((float)gPrefs.winSize_h);
				
				glm::vec3 lookDirInv = gInputState.m_clickCameraPos - gInputState.m_clickCameraInterest;
				glm::vec3 lookDirCrossAxis = glm::normalize (glm::cross ( gPrefs.cam_up, lookDirInv));
				gPrefs.cam_pos = gInputState.m_clickCameraPos + lookDirCrossAxis * offset_w * gPrefs.pan_step;
				gPrefs.cam_interest = gInputState.m_clickCameraInterest + lookDirCrossAxis * offset_w * gPrefs.pan_step;

				gPrefs.cam_pos = gPrefs.cam_pos + glm::normalize (glm::cross(lookDirCrossAxis,lookDirInv))  * offset_h * gPrefs.pan_step;
				gPrefs.cam_interest = gPrefs.cam_interest + glm::normalize (glm::cross(lookDirCrossAxis,lookDirInv))  * offset_h * gPrefs.pan_step;
				
				break;
			}
		}

		// upd camera
		glMatrixMode(GL_MODELVIEW) ;
		glLoadIdentity() ;
		gPrefs.defaultPrefsLookAt();
		glutPostRedisplay() ;
	}
} // Callbacks::MouseDragEven


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Callbacks::RegularKeys (unsigned char key,int x,int y) 
{
	switch(key) 
	{
		case '+':
			++ gPrefs.FOV;
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(gPrefs.FOV, (float)gPrefs.winSize_w/(float)gPrefs.winSize_h,gPrefs.near_clip, gPrefs.far_clip);
			glViewport(0,0,gPrefs.winSize_w,gPrefs.winSize_h);
			break;

		case '-':
			-- gPrefs.FOV ;
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(gPrefs.FOV, (float)gPrefs.winSize_w/(float)gPrefs.winSize_h,gPrefs.near_clip, gPrefs.far_clip);
			glViewport(0,0,gPrefs.winSize_w,gPrefs.winSize_h);
			break;

		case 'z': // default camera SRT 
			gPrefs.cam_pos = glm::normalize( gPrefs.cam_pos - gPrefs.cam_interest ) * gPrefs.restInterestDistance;
			gPrefs.cam_interest = gPrefs.cam_restPos;
			gPrefs.cam_up = glm::vec3(0.0, 1.0, 0.0 );
			break;

		case 'g': // hide\show home grid
			gPrefs.drawHomeGrid = !gPrefs.drawHomeGrid;
			break;

		case 'h': // print help
			PrintHelp();
			break;

		case 27:  // Escape to quit
			exit(0);
			break;

		
		default:
			break;
	}
	if ( key >= '0' && key <= '9' )
		gPrefs.maxDepthDraw = key - '0' + 1;
	if ( key == '-')
		gPrefs.maxDepthDraw = 100500;

	//redraw
	glutPostRedisplay(); 

} // Callbacks::RegularKeys


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Callbacks::ReshapeWindow ( int in_w,int in_h)
{
	gPrefs.winSize_w = in_w;
	gPrefs.winSize_h = in_h;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(gPrefs.FOV, (float)gPrefs.winSize_w/(float)gPrefs.winSize_h,gPrefs.near_clip, gPrefs.far_clip);
	glViewport(0,0,gPrefs.winSize_w,gPrefs.winSize_h);
	
	gScene->invalidateCachedPositions();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace GL{
} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
