#include "PTESources/PCH.hpp"

#include "PTESources/GLViewport_routines.hpp"
#include "PTESources/RenderSystem/BVH.hpp"
#include "PTESources/RenderSystem/GeometryBuilders.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace PTE;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void testBVH( BVHNode* root, float time, int trigsCount  );

void testRandomBVH( int maxTrigsPerTree );

void buildCar(float scale = 10 );

void buildCornellBox();

void buildXSpheres();


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

int main( int argc , char **argv )
{
	srand (time(NULL));

	Vec3 n( 0,0, 1 ), v( -1,0,-1 );
	v.voidNormalize();
	Vec3 refl = reflect( v, n );
	
	//reflect

	try
	{			
 		Scene scene( "smap6.bmp" );
		Camera camera;

		gScene = &scene;
		gCamera = &camera;
	
		/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

		std::vector< Vec3 > outVertices;
		std::vector< int > outIndexes;

		std::vector <Vec3> outNormals; // three serial vec3 for each triangle
		std::vector <Vec3> outUVWs; // three serial vec3 for each triangle
		std::vector <LONG> outMatIDperFace; // resulted length is equal to number of polygons\triangles
		


 		Color cSun(0.9f,0.8f,0.78f);
// 
// 		gScene->addLight( Light( Vec3(-5,+18,5), c1*0.9, 1 ) );
// 		gScene->addLight( Light( Vec3( 15,-15,-15), c2*0.9, 1 ) );
// 

 		Light sun( Vec3( 10E+5f , 10E+5f , 10E+5f ), cSun, 100 );
 		sun.doubleAttenuationDist = 100500500.f;

	//	gScene->addLight( sun );
		char * p_geometryModelPath = 0;
		p_geometryModelPath = "top2.ifxgt4";
		p_geometryModelPath = "top.ifxgt4";
		
	//	p_geometryModelPath = "buddha1.ifxgt4";
		p_geometryModelPath = "1m_tree.ifxgt4";
 		p_geometryModelPath = "xsiMan.ifxgt4";
 		ReadGT4Topology( p_geometryModelPath, outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
		gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes, 3, 0, Vec3(-40,-10, 0)  );


		p_geometryModelPath = "dragon1.ifxgt4";
		ReadGT4Topology( p_geometryModelPath, outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	//	gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes, 1, 0, Vec3(0,-18,0) );

	//	buildCar();
	//	buildCornellBox();
	//	buildXSpheres();

		GL::Timer timer; timer.reset();
		gScene->buildStaticGeom();
		testBVH( gScene->getTree(), timer.getElapsedTime(), gScene->getTrianglesCount() );


		// init GLUT and create Window
		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA |  GLUT_STENCIL);
		glutInitWindowPosition(200,200);
		glutInitWindowSize( GL::gPrefs.winSize_w, GL::gPrefs.winSize_h );
		glutCreateWindow("PTE"); 

		// register callbacks
		glutDisplayFunc(GL::Callbacks::DisplayScene);
		glutSpecialFunc(GL::Callbacks::SpecialKeys);
		glutKeyboardFunc(GL::Callbacks::RegularKeys);
		glutReshapeFunc(GL::Callbacks::ReshapeWindow); 
		glutReshapeWindow(GL::gPrefs.winSize_w, GL::gPrefs.winSize_h);
		glutMouseFunc(GL::Callbacks::MouseClickEvent) ;
		glutMotionFunc(GL::Callbacks::MouseDragEvent) ;
		//glutPassiveMotionFunc(GL::Callbacks::MousePureDragEvent) ; // in case of mouse moves without buttons pressed

		// init rest of data
		GL::Callbacks::InitFrame ();
	
		glutMainLoop();

	}
	catch( std::exception& e )
	{
		std::cout<<std::endl<<" std::exception arrived: "<<e.what();
	}
	catch( ... )
	{
		std::cout<<std::endl<<" undiagnosted exception arrived: ";
	}	


	return 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void testBVH( BVHNode* root, float time, int trigsCount )
{	
	BVHDiagnostic diag = BVHDiagnostic::traverse( *root, trigsCount );

	std::cout<< "build TIME               !!!=    "<<time					<< std::endl<<std::endl;
	std::cout<< "SIZE: total trigs count     =    "<<trigsCount				<< std::endl;
	std::cout<< "in-build trig Ptr's         =    "<<diag.trigsPtrCount		<< "\tideal == 1.0, real = "<< diag.trigsPtrCount / float(trigsCount) << std::endl<< std::endl;
	std::cout<< "build Nodes count           =    "<<diag.nodesCount		<< "\tideal >= 2.0, real = "<< diag.nodesCount / float(trigsCount) << std::endl;	
	std::cout<< "max trigs per node        !!=    "<<diag.maxTrigsPerNode	<< std::endl;
	std::cout<< "max depth                  !=    "<<diag.maxDepth			<< std::endl<< std::endl;

	for( int i=0; i<BVHDiagnostic::maxCount; i++ )
	{
		if ( diag.trigsPerNodeCount[i] != 0 )
		{
			std::cout<< i <<"=trigsNode\t=\t" << diag.trigsPerNodeCount[i]<< std::endl;
		}
	}
	std::cout <<std::endl;
	std::cout << "extra-size trigsNode\t=\t"<<diag.extraTrigsNodesCount<< std::endl;
	std::cout << "/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/";
	std::cout <<std::endl<< std::endl<< std::endl<< std::endl;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void buildCornellBox()
{

	Color c1(0.7f,0.7f,0.7f);
	Color c2(0.8,0.5f,0.3f);
	Color c3(0.05f,0.1f,0.7f);
	Color cSun(0.9f,0.8f,0.78f);

 	gScene->addLight( Light( Vec3(-25,+18,25), c1*0.9, 1 ) );
 	//gScene->addLight( Light( Vec3( 3,-5,-3), c2*10, 10 ) );
	//gScene->addLight( Light( Vec3( 19,0,22), c2*100.9, 0.9 ) );
//	gScene->addLight( Light( Vec3( 15,-15, 0), c3*0.6, 1 ) );
	// for correct use testing - no penetration by rays
	//gScene->addLight( Light( Vec3( 5,35, 5), c2*99999990.9, 1 ) );

	EmitTexture* emit = new EmitTexture( ColorA( 1,1,1, 1 ), 2 );
	ColorTexture* simpleDiff = new ColorTexture( ColorA( 0.5,0.4,0.5, 1 ) );
	ColorTexture* diffuseTile1 = new ColorTexture( "tile1.bmp" );
	ColorTexture* diffuseTile2 = new ColorTexture( "tile2.bmp" );
	ColorTexture* diffuseTile3 = new ColorTexture( "tile3.bmp" );
	ColorTexture* diffuseTile4 = new ColorTexture( "tile4.bmp" );
	ColorTexture* diffuseTile5 = new ColorTexture( "tile5.bmp" );
	ColorTexture* diffuseTransparent = new ColorTexture( ColorA( 1,1,1, 0 ) );
	NormalTexture* normal1 = new NormalTexture( "normals.bmp" );

	SurfaceMaterial* matTopBot = new SurfaceMaterial( normal1, diffuseTransparent, & gScene->mDefaultEmit );
	matTopBot->reflectK = 1;
	
	SurfaceMaterial* matBack = new SurfaceMaterial(normal1, simpleDiff, & gScene->mDefaultEmit );
	SurfaceMaterial* matFront = new SurfaceMaterial(0/*normal1*/, simpleDiff, & gScene->mDefaultEmit );
	SurfaceMaterial* matLeftRight = new SurfaceMaterial(0/*normal1*/, simpleDiff, & gScene->mDefaultEmit );
	SurfaceMaterial* matTopLight = new SurfaceMaterial(0/*normal1*/, diffuseTransparent, emit );
	float eps = 0.1f;
	
	float planeSize = 20;
	float lightSize = 20;
	float k = 40;

// 	gScene->addFlatQuad(
// 			Vec3(-lightSize, +planeSize - 2*eps, -lightSize)
// 		,	Vec3(-lightSize, +planeSize - 2*eps, lightSize)
// 		,	Vec3( lightSize, +planeSize - 2*eps, lightSize)
// 		,	Vec3( lightSize, +planeSize - 2*eps, -lightSize)	
// 		,	matTopLight
// 	);
	
	
// 	gScene->addFlatQuad(
// 			Vec3(-planeSize, -planeSize + eps, -planeSize)
// 		,	Vec3(-planeSize, -planeSize + eps, planeSize)
// 		,	Vec3( planeSize, -planeSize + eps, planeSize)
// 		,	Vec3( planeSize, -planeSize + eps, -planeSize)	
// 		,	matLeftRight
// 	);
// 
// // 	gScene->addFlatQuad(
// // 			Vec3(-planeSize, +planeSize - eps, -planeSize)
// // 		,	Vec3(-planeSize, +planeSize - eps, planeSize)
// // 		,	Vec3( planeSize, +planeSize - eps, planeSize)
// // 		,	Vec3( planeSize, +planeSize - eps, -planeSize)	
// // 		,	matLeftRight
// // 	);
// 
// 	
// 	gScene->addFlatQuad(
// 			Vec3(-planeSize, -planeSize , -planeSize + eps )
// 		,	Vec3(-planeSize, planeSize  , -planeSize + eps )
// 		,	Vec3( planeSize, planeSize  , -planeSize + eps )
// 		,	Vec3( planeSize, -planeSize , -planeSize + eps )	
// 		,	matLeftRight
// 	);
// 
// 	gScene->addFlatQuad(
// 			Vec3(-planeSize, -planeSize , +planeSize - eps )
// 		,	Vec3(-planeSize, planeSize  , +planeSize - eps )
// 		,	Vec3( planeSize, planeSize  , +planeSize - eps )
// 		,	Vec3( planeSize, -planeSize , +planeSize - eps )	
// 		,	matLeftRight
// 	);
// 
// 	gScene->addFlatQuad(
// 			Vec3(-planeSize + eps, -planeSize,-planeSize)
// 		,	Vec3(-planeSize + eps,-planeSize, planeSize)
// 		,	Vec3(-planeSize + eps, planeSize, planeSize)
// 		,	Vec3(-planeSize + eps, planeSize,-planeSize)	
// 		,	matFront
// 	);

// 		gScene->addFlatQuad(
// 				Vec3( 30,-planeSize*2,-planeSize*2)
// 			,	Vec3( 30,-planeSize*2, planeSize*2)
// 			,	Vec3( 30, planeSize*2, planeSize*2)
// 			,	Vec3( 30, planeSize*2,-planeSize*2)	
// 			,	matBack
// 		);

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	std::vector< Vec3 > outVertices;
	std::vector< int > outIndexes;

	std::vector <Vec3> outNormals; // three serial vec3 for each triangle
	std::vector <Vec3> outUVWs; // three serial vec3 for each triangle
	std::vector <LONG> outMatIDperFace; // resulted length is equal to number of polygons\triangles


	ReadGT4Topology( "cornellBoxTriangulated.ifxgt4", outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
//	gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes );

	SurfaceMaterial* matSphere1 = new SurfaceMaterial( 0, diffuseTransparent, & gScene->mDefaultEmit );
	matSphere1->refractK = 1;
	//matSphere1->reflectK = 1;
	matSphere1->ior = 0.8;
	matSphere1->glossRefract = 0.999999;
	matSphere1->refractC.b = 0.9f;

	SurfaceMaterial* matSphere2 = new SurfaceMaterial( 0, diffuseTransparent, & gScene->mDefaultEmit );
	//matSphere2->refractK = 1;
	matSphere2->reflectK = 1;
	matSphere2->ior = 0.9;
	matSphere2->glossReflect = 1;
	
	buildSphere( outVertices, outIndexes, outNormals,  2, 3, 5 );
	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 5, matSphere2, Vec3(  0,  28,  0  ) );
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 5, matSphere1, Vec3( -68,  8, -68  ) );
//  	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 5, matSphere1, Vec3( 10, -9,  10 ) );
}



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void buildXSpheres()
{
	std::vector< Vec3 > outVertices;
	std::vector< int > outIndexes;

	std::vector <Vec3> outNormals; // three serial vec3 for each triangle
	std::vector <Vec3> outUVWs; // three serial vec3 for each triangle
	std::vector <LONG> outMatIDperFace; // resulted length is equal to number of polygons\triangles

	ColorTexture* diffuseTransparent = new ColorTexture( ColorA( 1,1,1, 0 ) );
	SurfaceMaterial* matSphere1 = new SurfaceMaterial( 0, diffuseTransparent, & gScene->mDefaultEmit );
	matSphere1->refractK = 1;
	//matSphere1->reflectK = 1;
	matSphere1->ior = 0.96f;
	matSphere1->glossRefract = 0.9999;
	matSphere1->refractC.r = 1;

	SurfaceMaterial* matSphere2 = new SurfaceMaterial( 0, diffuseTransparent, & gScene->mDefaultEmit );
	matSphere2->reflectK = 1;
	matSphere2->glossReflect = 1;
/*	matSphere2->reflectC.g = 1;*/

	buildSphere( outVertices, outIndexes, outNormals, 20, 20, 10 );
	
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 0, - 25, 0 ) );
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 0, - 20, 0 ) );
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 0, - 15, 0 ) );
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 0, - 10, 0 ) );
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 0, - 5, 0 ) );
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 0, - 0, 0 ) );
// 
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 0,  5, 0 ) );
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 0,  10, 0 ) );
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 0,  15, 0 ) );
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 0,  20, 0 ) );
// 	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 0,  25, 0 ) );

	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere1, Vec3( -5, 25, 5 ) );
	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere1, Vec3( 35, 0, 0 ) );
	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere1, Vec3( 25, -30, -55 ) );
	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere1, Vec3( 50, -25, -25 ) );
	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere1, Vec3( -30, -0, -25 ) );
	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere1, Vec3( -50, 10, -55 ) );

	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 5, 15, -10 ) );
	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 50, 45, -50 ) );
	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( -15, 55, -15 ) );
	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3( 65,-0, -5 ) );
	gScene->inputSoftImage_StaticGeometry( outVertices, &outNormals, 0, outIndexes, 1, matSphere2, Vec3(  5,-25, -5 ) );
 }


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



void buildCar( float scale )
{
	Color cSun(0.5f,0.5,0.5f);

	// inner-salon light!
	//gScene->addLight( Light( Vec3(+0,+0.9,+0), cSun*0.9, 0 ) );
	Light sun( Vec3( 10E+5f , 10E+5f , 10E+5f ), cSun, 100 );
	sun.doubleAttenuationDist = 100500500.f;

	gScene->addLight( sun );
	
	ColorTexture* diffuse11 = new ColorTexture( "crate.bmp" );
	ColorTexture* diffuseSimple11 = new ColorTexture( ColorA( 1,1,1, 1 ) );
	NormalTexture* normal1 = new NormalTexture( "normals.bmp" );

	SurfaceMaterial* matGround = new SurfaceMaterial();
	matGround->mNormalTex = normal1;
	matGround->mDiffTex = diffuseSimple11;
	matGround->mEmitTex = & gScene->mDefaultEmit;
	matGround->reflectC = Color( 1.f, 1.f, 1.f );
	matGround->refractC = Color( 1.f, 1.f, 1.f);
	matGround->reflectK = 0.5;
	matGround->refractK = 0;
	matGround->ior = 0.9f;
	matGround->glossReflect = 1;
	matGround->glossRefract = 0.99f;

	float planeSize = 40 * scale;
	Triangle tR1(
			Vec3(-planeSize,-0, planeSize)
		,	Vec3( planeSize,-0, planeSize)
		,	Vec3( planeSize,-0,-planeSize)
		);
	tR1.setUV( 0,1,1, 1,1,0 );

	Triangle tR2(
			Vec3(  planeSize,-0, -planeSize)
		,	Vec3( -planeSize,-0, -planeSize)
		,	Vec3( -planeSize,-0,  planeSize)
		);
	tR2.setUV( 1,0,0, 0,0,1 );

	tR1.material = matGround;	tR2.material = matGround;

	gScene->addTriangle( tR1 );
	gScene->addTriangle( tR2 );


	std::vector< Vec3 > outVertices;
	std::vector< int > outIndexes;

	std::vector <Vec3> outNormals; // three serial vec3 for each triangle
	std::vector <Vec3> outUVWs; // three serial vec3 for each triangle
	std::vector <LONG> outMatIDperFace; // resulted length is equal to number of polygons\triangles

	ColorTexture* diffuseSimple1 = new ColorTexture( ColorA( 1, 0.1, 0.1, 1 ) );
	SurfaceMaterial* matHull = new SurfaceMaterial();
	matHull->mNormalTex = 0;
	matHull->mDiffTex = diffuseSimple1;
	matHull->mEmitTex = & gScene->mDefaultEmit;
	matHull->reflectC = Color( 1.f, 1.f, 1.f );
	matHull->refractC = Color( 1.f, 1.f, 1.f );
	matHull->reflectK = 1;
	matHull->refractK = 0;
	matHull->ior = 0.9f;
	matHull->glossReflect = 1;
	matHull->glossRefract = 1;
	ReadGT4Topology( "body_carpaint1.ifxgt4", outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes, scale, matHull );


	ColorTexture* diffuseSimple2 = new ColorTexture( ColorA( 0.5, 0.2, 0.3, 1 ) );
	SurfaceMaterial* matWheel = new SurfaceMaterial();
	matWheel->mNormalTex = 0;
	matWheel->mDiffTex = diffuseSimple2;
	matWheel->mEmitTex = & gScene->mDefaultEmit;
	matWheel->reflectC = Color( 1.f, 1.f, 1.f );
	matWheel->refractC = Color( 1.f, 1.f, 1.f );
	matWheel->reflectK = 5;
	matWheel->refractK = 0;
	matWheel->ior = 0.9f;
	matWheel->glossReflect = 1;
	matWheel->glossRefract = 0.99f;

	ReadGT4Topology( "wheels_steel1.ifxgt4", outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes, scale,matWheel );


	ColorTexture* diffuseSimple3 = new ColorTexture( ColorA( 0.0, 0.1, 0.1, 1 ) );
	SurfaceMaterial* matTire = new SurfaceMaterial();
	matTire->mNormalTex = 0;
	matTire->mDiffTex = diffuseSimple3;
	matTire->mEmitTex = & gScene->mDefaultEmit;
	matTire->reflectC = Color( 1.f, 1.f, 1.f );
	matTire->refractC = Color( 1.f, 1.f, 1.f );
	matTire->reflectK = 0;
	matTire->refractK = 0;
	matTire->ior = 1;
	matTire->glossReflect = 1;
	matTire->glossRefract = 1;

	ReadGT4Topology( "protectors1.ifxgt4", outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes,scale, matTire );


	ColorTexture* diffuseSimple4 = new ColorTexture( ColorA( 1, 1, 1, 1 ) );
	SurfaceMaterial* matSalon = new SurfaceMaterial();
	matSalon->mNormalTex = 0;
	matSalon->mDiffTex = diffuseSimple4;
	matSalon->mEmitTex = & gScene->mDefaultEmit;
	matSalon->reflectC = Color( 1.f, 1.f, 1.f );
	matSalon->refractC = Color( 1.f, 1.f, 1.f );
	matSalon->reflectK = 0;
	matSalon->refractK = 0;
	matSalon->ior = 1;
	matSalon->glossReflect = 1;
	matSalon->glossRefract = 1;
	ReadGT4Topology( "salon_black1.ifxgt4", outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes,scale, matSalon );

	

	ColorTexture* diffuseSimple5 = new ColorTexture( ColorA( 0,0,0, 0 ) );
	SurfaceMaterial* matGlass = new SurfaceMaterial();
	matGlass->mNormalTex = 0;
	matGlass->mDiffTex = diffuseSimple5;
	matGlass->mEmitTex = & gScene->mDefaultEmit;
	matGlass->reflectC = Color( 1.f, 1.f, 1.f );
	matGlass->refractC = Color( 1.f, 1.f, 1.f );
	matGlass->reflectK = 1;
	matGlass->refractK = 7;
	matGlass->ior = 1;
	matGlass->glossReflect = 1;
	matGlass->glossRefract = 1;
	ReadGT4Topology( "glass1.ifxgt4", outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes,scale, matGlass );


	ColorTexture* diffuseSimple6 = new ColorTexture( ColorA( 0.1,0.2,0.2, 1 ) );
	SurfaceMaterial* matMatte = new SurfaceMaterial();
	matMatte->mNormalTex = 0;
	matMatte->mDiffTex = diffuseSimple6;
	matMatte->mEmitTex = & gScene->mDefaultEmit;
	matMatte->reflectC = Color( 1.f, 1.f, 1.f );
	matMatte->refractC = Color( 1.f, 1.f, 1.f );
	matMatte->reflectK = 1;
	matMatte->refractK = 0;
	matMatte->ior = 1;
	matMatte->glossReflect = 1;
	matMatte->glossRefract = 1;
	ReadGT4Topology( "black_matte1.ifxgt4", outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes,scale, matMatte );



	ColorTexture* diffuseSimple7 = new ColorTexture( ColorA( 1,1,1, 1 ) );
	SurfaceMaterial* matLights = new SurfaceMaterial();
	matLights->mNormalTex = 0;
	matLights->mDiffTex = diffuseSimple7;
	matLights->mEmitTex = & gScene->mDefaultEmit;
	matLights->reflectC = Color( 1.f, 1.f, 1.f );
	matLights->refractC = Color( 1.f, 1.f, 1.f );
	matLights->reflectK = 0;
	matLights->refractK = 0;
	matLights->ior = 1;
	matLights->glossReflect = 1;
	matLights->glossRefract = 1;
	ReadGT4Topology( "lights_chrome1.ifxgt4", outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes,scale, matLights );


	ColorTexture* diffuseSimple8 = new ColorTexture( ColorA( 1,0,0, 1 ) );
	SurfaceMaterial* matBackLight = new SurfaceMaterial();
	matBackLight->mNormalTex = 0;
	matBackLight->mDiffTex = diffuseSimple8;
	matBackLight->mEmitTex = & gScene->mDefaultEmit;
	matBackLight->reflectC = Color( 1.f, 1.f, 1.f );
	matBackLight->refractC = Color( 1.f, 0.f, 0.f );
	matBackLight->reflectK = 0;
	matBackLight->refractK = 5;
	matBackLight->ior = 1;
	matBackLight->glossReflect = 1;
	matBackLight->glossRefract = 1;
	
	ReadGT4Topology( "redglass_backside1.ifxgt4", outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes,scale, matBackLight );
	

	ColorTexture* diffuseSimple9 = new ColorTexture( ColorA( 1,1,1, 1 ) );
	SurfaceMaterial* matWall = new SurfaceMaterial();
	matWall->mNormalTex = 0;
	matWall->mDiffTex = diffuseSimple9;
	matWall->mEmitTex = & gScene->mDefaultEmit;
	matWall->reflectC = Color( 1.f, 1.f, 1.f );
	matWall->refractC = Color( 1.f, 1.f, 1.f );
	matWall->reflectK = 0;
	matWall->refractK = 0;
	matWall->ior = 1;
	matWall->glossReflect = 1;
	matWall->glossRefract = 0.999;

	ReadGT4Topology( "wall1.ifxgt4", outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	gScene->inputSoftImage_StaticGeometry( outVertices, & outNormals, &  outUVWs, outIndexes, scale, matWall );


}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void testRandomBVH( int maxTrigsPerTree )
{
	Camera camera;

	gScene = 0;
	gCamera = &camera;

	int count = 2;
	while(1)
	{
		count *= 2;	
		if ( count > maxTrigsPerTree )
			return;
		
		GL::Timer timer;
		timer.reset();
		gScene = new Scene("smap1.bmp");


		float deviation = PTE::sqrt( count )  + 10;
		int countTrigs = count;
		for ( int i=0; i<countTrigs; i++ )
		{
			Vec3 v1( rand(-5,5),rand(-5,5),rand(-5,5) );
			Vec3 v2( rand(-5,5),rand(-5,5),rand(-5,5) );
			Vec3 v3( rand(-5,5),rand(-5,5),rand(-5,5) );

			Vec3 randTrans ( rand(-deviation,deviation),rand(-deviation,deviation),rand(-deviation,deviation) );
			v1+=randTrans;
			v2+=randTrans;
			v3+=randTrans;
			Triangle t( v1,v2,v3 );
			if( t.square== 0 )
				++countTrigs;
			else
				gScene->addTriangle( t );
		}

		
		Color c1(0.5f,0.7f,0.4f);
		Color c2(0.5f,0.78f,0.4f);
		c2 *= 10.5f;

		std::cout<< "/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/";
		std::cout<< "\npreparation TIME = " <<timer.getElapsedTime() << std::endl;
		timer.reset();
		gScene->buildStaticGeom();
		BVHNode * tree = gScene->getTree();
	
		testBVH( tree, timer.getElapsedTime(), count );

		delete tree;

	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

