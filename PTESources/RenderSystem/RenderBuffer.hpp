#pragma once
#include "PTESources/RenderSystem/Color.hpp"
#include <vector>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class RenderBuffer
{
public:

	friend class RenderBufferStack;

	RenderBuffer()
		:	mNbPixelsX ( -42 )
		,	mNbPixelsY ( -42 )
		,	mColorBuff( 0 )
		,	mSquareDepthBuff( 0 )
	{}

	void resize( int x, int y, Color* ptr, float* depth )
	{
		mNbPixelsX = x;
		mNbPixelsY = y;
		mColorBuff = ptr;
		mSquareDepthBuff = depth;
	}

	Color* color( int x, int y )
	{
		Assert (			
				x >= 0			
			&&	x < mNbPixelsX	
			&&	y >= 0			
			&&	y < mNbPixelsY	
		);
		return mColorBuff + y * mNbPixelsX + x;
	}

	Color* color( int i )
	{
		Assert( i >= 0  && i < mNbPixelsX * mNbPixelsY );
		return mColorBuff + i;
	}

	float* squareDepth( int x, int y )
	{
		Assert (			
				x >= 0			
			&&	x < mNbPixelsX	
			&&	y >= 0			
			&&	y < mNbPixelsY	
		);
		return mSquareDepthBuff + ( y * mNbPixelsX ) + x;
	}

	float* squareDepth( int i )
	{
		Assert( i >= 0  && i < mNbPixelsX * mNbPixelsY );
		return mSquareDepthBuff + i;
	}
	
	int xSize() const { return mNbPixelsX; }
	int ySize() const { return mNbPixelsY; }
	
public:

	Color* mColorBuff;
	float* mSquareDepthBuff;

private:

	int mNbPixelsX;
	int mNbPixelsY;

}; // class RenderBuffer


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class RenderBufferStack
{
public:

	RenderBufferStack()
	{
		mNbPixelsX = 0;
		mNbPixelsY = 0;
		mLayers = 0;

		mCommonSquareDepthBuff = 0;
		mCommonColorBuff = 0;
		mRenderBuffers = 0;
	}

	~RenderBufferStack()
	{
		free( mRenderBuffers );
		free( mCommonColorBuff );
		free( mCommonSquareDepthBuff );
	}

	bool resize( int layers, int x, int y );

	int xSize() const	{ return mNbPixelsX; }
	int ySize() const	{ return mNbPixelsY; }

	RenderBuffer* operator []( int i )
	{
		Assert( i < mLayers && i >= 0 );
		return mRenderBuffers + i;
	}

	int getLayersCount() const { return mLayers; }

	void swapBuffers( int layer1, int layer2 );

private:

	Color* getColorSlice( int layerNumber )
	{
		return mCommonColorBuff + mNbPixelsX * mNbPixelsY * layerNumber;
	}

	float* getSquareDepthSlice( int layerNumber )
	{
		return mCommonSquareDepthBuff + mNbPixelsX * mNbPixelsY * layerNumber;
	}
	
	void alloc();

private:

	Color* mCommonColorBuff;
	float* mCommonSquareDepthBuff;
	
	int mNbPixelsX;
	int mNbPixelsY;

	int mLayers;

	RenderBuffer* mRenderBuffers;

}; // class RenderBufferStack


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
