#pragma once

#include "PTESources/RenderSystem/Camera.hpp"
#include "PTESources/RenderSystem/Light.hpp"
#include "PTESources/Math/Intersection.hpp"
#include "PTESources/Math/Geometry.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// default correct - 0.0001f
#define TRIANGLE_RAYCAST_OFFSET 0.0001f


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Scene;
struct ColorTexture;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct RayTraceParams
{
	RayTraceParams( int diffDepth, int reflectDepth, int refractDepth, int diffSam, int reflSam, int refrSam, int perLightSample )
	{
		data[0] = diffDepth;
		data[1] = reflectDepth;
		data[2] = refractDepth;

		data[3] = diffSam;
		data[4] = reflSam;
		data[5] = refrSam;

		data[6] = perLightSample;
	}

	int maxDiffDepth() const			{ return data[0]; }
	int maxReflectDepth() const			{ return data[1]; }
	int maxRefractDepth() const			{ return data[2]; }

	int maxDiffSamples()	const		{ return data[3]; }
	int maxReflectSamples()	const		{ return data[4]; }
	int maxRefractSamples()	const		{ return data[5]; }

	int maxDirectLightSamples()	const	{ return data[6]; }
	

private:
	
	int data[8];

}; // struct RayTraceParams


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct SurfaceShader
{

public:

	SurfaceShader( Scene& s )	:	scene(s)	{}
	Color onGeometry(
			const VertexMaterial* prevVertexMat, const Intersection& sourceIsec
		,	const Ray& ray, const RayTraceParams& depth, float maxDist, int& rayCasts
	);

public:

	Scene& scene;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct EnvironmentShader
{
	virtual Color onRay( const Ray& outerRay ) = 0;
	virtual ~EnvironmentShader(){}
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct DefaultEnvironmentShader
	:	public EnvironmentShader
{
	virtual Color onRay( const Ray& outerRay );
	
	DefaultEnvironmentShader( const char* name )
		:	map(name)
	{}

	Spheremap map;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
