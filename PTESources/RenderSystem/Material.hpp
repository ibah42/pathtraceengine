#pragma once

#include "PTESources/RenderSystem/Texture.hpp"
#include "PTESources/RenderSystem/Color.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct VertexMaterial
{

	void normalize()
	{
		glossReflect = clamp( 0, 1, glossReflect );
		glossRefract = clamp( 0, 1, glossRefract );

		float d = 1.f / sqrt( reflectK*reflectK + refractK*refractK + diffK*diffK );
		reflectK *= d;
		refractK *= d;
		diffK *= d;
	}

public:

	Vec3 vertNormal;
	float reflectK, refractK, diffK;
	float ior;
	float glossReflect, glossRefract;

	// Color-filters
	Color refractC, reflectC;

	// true-colors
	Color diffC, emitC;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct SurfaceMaterial
{
	SurfaceMaterial(){}

	SurfaceMaterial( NormalTexture* ntex, ColorTexture* diffTex, EmitTexture* emitTex )
	{
		mEmitTex = emitTex;
		mNormalTex = ntex;
		mDiffTex = diffTex;

		reflectC = Color( 1, 1, 1 );
		refractC = Color( 1, 1, 1 );
		reflectK = 0;
		refractK = 0;
		ior = 1;
		glossReflect = 1;
		glossRefract = 1;
	}
	

	void getVertexMaterial( VertexMaterial& out, float u, float v, const Vec3& usedTrigNormal ) const
	{
		ColorA emit = mEmitTex->onFilterBiLinear( u, v );
		out.emitC = emit.c;
		out.emitC *= emit.alpha;

		ColorA diffColor = mDiffTex->onFilterBiLinear( u, v );
		out.diffC = diffColor.c;
		out.diffK = diffColor.alpha;

		if ( mNormalTex )
			out.vertNormal = mNormalTex->onFilterBiLinear( u, v, usedTrigNormal );
		else
			out.vertNormal = usedTrigNormal;

		out.glossReflect = glossReflect;
		out.glossRefract = glossRefract;

		out.ior = ior;
		out.reflectK = reflectK;
		out.refractK = refractK;
		
		out.reflectC = reflectC;
		out.refractC = refractC;

		out.normalize();
	}

public:

	NormalTexture* mNormalTex;
	ColorTexture* mDiffTex;
	EmitTexture* mEmitTex;

	float reflectK, refractK;
	float ior;
	float glossReflect, glossRefract;

	// Color-filters
	Color refractC, reflectC;
	
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
