#pragma once

#include "PTESources/Math/Vector3.hpp"
#include "PTESources/Math/Quaternion.hpp"
#include "PTESources/RenderSystem/Light.hpp"
#include "PTESources/RenderSystem/Camera.hpp"
#include "PTESources/RenderSystem/BVH.hpp"
#include "PTESources/RenderSystem/Shaders.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

struct BVHNode;
struct EnvironmentShader;
struct PhongSurfaceShader;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Scene
{
public:

	Scene( const char* envTexName );

	void invalidateCachedPositions()				{ mSceneChanged = true; }

	int getLightsCount()							{ return (int) mLights.size(); }
	const Light& getLight( int i ) const			{ return mLights[i]; }
	Light& getLight( int i )						{ return mLights[i]; }
	void addLight( const Light& l )					{ mLights.push_back( l ); }
	
	const Triangle& getTriangle( int i ) const		{ return mBoundedTriangles[ i ].trig; }
	int getTrianglesCount()							{ return (int)mBoundedTriangles.size(); }

	int addTriangle( const Triangle& t )
	{
		Assert( t.square > 0 );
		mBoundedTriangles.push_back( BoundedTriangle( t ) );
		return (int)mBoundedTriangles.size() - 1;
	}

	void releaseTriangles()							{ mBoundedTriangles.clear(); }

	~Scene()										{ releaseTriangles(); }

	void inputSoftImage_StaticGeometry(
			const std::vector< Vec3 >& vertices, const std::vector< Vec3 >* normals, const std::vector< Vec3 >* uvw
		,	const std::vector< int >& indexes, float scale = 1, const SurfaceMaterial* material = 0, Vec3 disp = Vec3(0,0,0), const int maxTrigs = 1000*1000*1000
	);
	
	// false == no intersection( point intersected freely )
	// true == something intersected
	bool rayCastPoint( const Vec3& origin, const Vec3& point, const BoundedTriangle* ignore);
	void rayCast( Intersection& result, const Ray& ray, float maxQuadDist, const BoundedTriangle* ignore);
		
	// false == no intersection
	// true == something intersected
	bool rayCastAny( const Ray& ray, const BoundedTriangle* ignore );

	void update( Camera& camm, float dt );
	void buildStaticGeom();
	BVHNode* getTree() { return mStaticGeom->mBVHNodes; }

	Color geometryShaderInvoke( const Ray& ray, const BoundedTriangle* ignore, const RayTraceParams& currDepth );

	// at first - intersect with triangle - if missed - make truecast
	Color geometryShaderInvokePrimeCast( const Ray& ray, const BoundedTriangle& prime, const RayTraceParams& currDepth );

	void addFlatQuad( const Vec3& _00, const Vec3& _01, const Vec3& _11, const Vec3& _10, SurfaceMaterial* mat );

private:

	Color geometryShaderInvokeWithDepth( const Ray& ray, const BoundedTriangle* ignore, const RayTraceParams& currDepth, float& squareDepth );

	Color adaptiveAA( Camera& camera, int x, int y, int sourceLayer, int beams, float& depth );
	Color renderBeam( Camera& cam, int x, int y, const int beams, float& depth );
	void render( Camera& camera );

public:
	
	int mAACount;
	float maxRGB_AASample_derivation;
	float maxRGB_AADegradation_derivation;
	RayTraceParams mRayTraceParams; 
	std::auto_ptr< EnvironmentShader > mEnvShader;
	SurfaceShader mSurfaceShader;
	
	EmitTexture mDefaultEmit;
	ColorTexture mDefaultDiff;
	SurfaceMaterial mDefaultMaterial;

private:
	
	int mRaysPerFrame;
	bool mSceneChanged;
	int mCurrentAAIteration;
	BVHPacked* mStaticGeom;
	Vec3 mLightRotCenter;
	float mLightRotRadius;
	Angle mLightRot;
	
	std::vector< BoundedTriangle > mBoundedTriangles;
	std::vector< Light > mLights;

}; // class Scene


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
