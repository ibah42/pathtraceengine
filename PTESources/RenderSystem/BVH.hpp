#pragma once

#include "PTESources/Math/Geometry.hpp"
#include "PTESources/RenderSystem/BTVec.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

struct BVHNode;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct BVHDiagnostic
{
	static BVHDiagnostic traverse( const BVHNode& root, int trigsTotalCount );

private:

	void visit( const BVHNode& node, int depth );
	
	BVHDiagnostic()
	{
		nodesCount = 0;
		trigsPtrCount = 0;
		maxTrigsPerNode = 0;

		memset( &trigsPerNodeCount, 0, sizeof( int ) * maxCount );

		extraTrigsNodesCount = 0;
		maxDepth = 0;
	}

public:

	static const int maxCount = 1024*8;


	std::set< const BoundedTriangle* > detectedTrigs;

	int nodesCount;
	int trigsPtrCount;
	int maxTrigsPerNode;
	
	int trigsPerNodeCount[maxCount];

	int extraTrigsNodesCount;
	int maxDepth;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

struct BVHPacked;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct BVHNode
{
	static BVHPacked* buildPackedBVH( BTVec& trigs );	
	__host__ __device__ void traceNearest( const Ray& ray, Intersection& out, float maxQuadDist, const BoundedTriangle* ignore);
	__host__ __device__ bool traceAny( const Ray& ray, float maxQuadDist, const BoundedTriangle* ignore );
	
	~BVHNode()
	{
		//EMPTY!!!!
	}

	enum Axis { x=0, y=1, z=2 };

private:
	
	static BVHNode*	splitNodeByMedian( const BTVec& trigs, BTVec& trigsLeft, BTVec& trigsRight );
	static BVHNode*  buildBVHNode( const BTVec& trigs, int currentDepth = 0 );	
	static BVHNode * buildBVHLeaf( BTVec& trigs );	
	static void build( BVHNode& node, const Vec3& minBound, const Vec3& maxBound );
	static void build( BVHNode& node, const AABB& box );

	__host__ __device__ void traceBrutalNearestTrigs ( Intersection& out, const Ray& ray, const BTVec& vec, float maxQuadDist, const BoundedTriangle* ignore );
	__host__ __device__ bool traceBrutalAnyTrigs ( const Ray& ray, const BTVec& vec, float maxQuadDist, const BoundedTriangle* ignore );
	

public:

	AABB mBounds;
	BVHNode* mLeft;
	BVHNode* mRight;

	BTVec mTrigs;

}; // struct BVHNode


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct BVHPacked
{
	friend struct BVHNode;

	BVHPacked()
	{
		mBVHNodes = 0;
		mBVHNodesCount = 0;
		mCurrentBuildingBVHNode = 0;
		mCurrentBuildingTrigPtr = 0;
	}

	BVHPacked( BVHPacked& that )
	{
		mTrigs.setZero();
		mTrigs.swap( that.mTrigs );
		mBVHNodes = that.mBVHNodes;
		mBVHNodesCount = that.mBVHNodesCount;

		mCurrentBuildingTrigPtr = 0;
		mCurrentBuildingBVHNode  = 0;
		
		that.mBVHNodes = 0;
		that.mBVHNodesCount = 0;
		that.mTrigs.setZero();		
	}

	void visitGatherData( BVHNode& node );
	BVHNode* visitCopyData( const BVHNode& nodeSource );

	~BVHPacked()
	{
		if ( mBVHNodes)
			free( mBVHNodes );
		
		mTrigs.die();
	}
private:

	void killNotPacked( BVHNode& node );

public:

	BTVec mTrigs;
	BVHNode* mBVHNodes;
	int mBVHNodesCount;

private:

	const BoundedTriangle** mCurrentBuildingTrigPtr;
	BVHNode* mCurrentBuildingBVHNode;

};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
