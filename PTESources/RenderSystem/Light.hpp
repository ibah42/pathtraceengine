#pragma once

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct Light
{
	Light(  Vec3 v, Color c, float r = 0 )
		:	position( v )
		,	color( c )
		,	radius( r )
	{
		doubleAttenuationDist = 30;
	}

	Color getResultPowerOnDist( float dist ) const
	{
		dist -= radius;
		if ( dist <= 0 )
			return color;

		float r = clamp( 0.0f, 1.0f, 1 - dist / 2 / doubleAttenuationDist );
		return color * r;
	}

	Vec3 sampleRandomDirectHemiSphere( const Vec3& rayStart ) const
	{
		Vec3 res( randf_11(), randf_11(), randf_11() );
		res.voidNormalize();
				
		Vec3 dir = position - rayStart;
		dir.voidNormalize();

		if ( dir.dot( res ) > 0 )
			res = - res;

		res *= radius;
		res += position;
		return res;
	}

public:

	Color color;
	float doubleAttenuationDist;
	Vec3 position;

	// if radius == 0 - no light sampling;
	float radius;

}; // class Light


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
