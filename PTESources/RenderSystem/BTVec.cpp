#include "PTESources/PCH.hpp"
#include "PTESources/RenderSystem/BTVec.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void BTVec::die()
{
	if( mTrigs )
		free( mTrigs );

	mTrigs = 0;	
	mCount = 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


const BoundedTriangle& BTVec::operator() ( int id ) const
{
	Assert( id >= 0 && id < mCount );
	return *( mTrigs[id] );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


const BoundedTriangle** BTVec::operator[] ( int id ) const
{
	Assert( id >= 0 && id < mCount );
	return mTrigs+id;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


const BoundedTriangle& BTVec::top() const
{
	return *( mTrigs[ mCount - 1 ] );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


const BoundedTriangle& BTVec::bot() const
{
	return *( mTrigs[ 0 ] );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void BTVec::swap( BTVec& that )
{
	int count = mCount;
	const BoundedTriangle** trigs = mTrigs;

	mCount = that.mCount;
	mTrigs = that.mTrigs;

	that.mCount = count;
	that.mTrigs = trigs;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BTVec::BTVec( BTVec& that )
{
	mCount = 0;
	mTrigs = 0;
	swap( that );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BTVec::BTVec()
{
	mCount = 0;
	mTrigs = 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BTVec BTVec::buildFromArray( const BoundedTriangle* array, int count )
{
	BTVec result( count );
	
	for( int i=0; i<count; ++i )
	{
		result.mTrigs[i] = & array[i];
	}
	
	return result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BTVec::BTVec( int count, const BoundedTriangle** trigs )
{
	mTrigs = 0;
	alloc( count );
	memcpy( mTrigs, trigs, count * sizeof( const BoundedTriangle* ) );
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BTVec::BTVec( int count )
{
	mTrigs = 0;
	alloc( count );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void BTVec::copy( const BTVec& that )
{
	int count = that.mCount;
	if( count < 1 )
		return;

	die();
	alloc( count );
	mCount = count;

	memcpy( mTrigs, that.mTrigs, sizeof( const BoundedTriangle* ) * count );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void BTVec::copy( const BoundedTriangle** trigs )
{
	Assert(mCount && mTrigs);

	memcpy( mTrigs, trigs, sizeof( const BoundedTriangle* ) * mCount );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void BTVec::alloc( int count )
{
	die();

	if( count>0 )
	{
		mTrigs = (const BoundedTriangle**) malloc( count * sizeof( const BoundedTriangle*) );
		mCount = count;
	}
	else
	{
		mCount = 0;
		mTrigs = 0;
	}
	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BTVec::~BTVec()
{
	die();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
