#include "PTESources/PCH.hpp"
#include "PTESources/RenderSystem/BVH.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define STOP_SPLIT_TRIGS_PER_NODE_COUNT 2
#define MAX_DEPTH 36
#define MAX_SAH 9E+35f

#define STOP_FIRST_K 0.85f
#define STOP_SECOND_K 0.83f

#define SAH_STOP_AT_SPLITING_K 1.0f
#define MAX_SPLIT_DECREASE_COUNT 2

#define SAH_ACTIVE_COUNT 50

	
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < BVHNode::Axis axis >
inline double getSAH( const BTVec& vec, const float orthValue, int& leftCount, int& rightCount )
{
	leftCount = 0;
	rightCount = 0;
	double leftSquare = 0;
	double rightSquare = 0;
	double totalSquareSAH = 0;

	for( int i=0; i<vec.mCount; ++i )
	{
		int leftTC = 0 , rightTC = 0;
		splitTriangle< axis >( vec(i), leftTC, rightTC, orthValue );

		float square = vec(i).trig.getSquare();
		if ( leftTC > 0 )
		{
			++leftCount;
			leftSquare += square;
		}
		if ( rightTC > 0 )
		{
			++rightCount;
			rightSquare += square;
		}

//		totalSquareSAH += square;
	}

	//totalSquareSAH *= vec.mCount;

	double result = leftCount*leftSquare + rightCount*rightSquare;
	
	return result;

} //getSAH 


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


AABB inline getAABB( const BTVec& trigs )
{
	int totalCount = trigs.mCount;

	float xmin, ymin, zmin;
	float xmax, ymax, zmax;
	{
		const Vec3& min = trigs(0).box.min();
		const Vec3& max = trigs(0).box.max();

		xmin = min.x; ymin = min.y; zmin = min.z;
		xmax = max.x; ymax = max.y; zmax = max.z;
	}

	for ( int i = 1; i<totalCount; i++ )
	{
		const Vec3& min = trigs(i).box.min();
		const Vec3& max = trigs(i).box.max();

		xmin = minimal( xmin, min.x );
		ymin = minimal( ymin, min.y );
		zmin = minimal( zmin, min.z );

		xmax = maximal( xmax, max.x );
		ymax = maximal( ymax, max.y );
		zmax = maximal( zmax, max.z );		
	}

	return AABB( xmin, ymin, zmin, xmax, ymax, zmax );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < BVHNode::Axis axis >
inline void
splitByAxis( const BTVec& in, BTVec& left, BTVec& right, const float orthValue )
{
	// can be empty BVHNode!
	Assert( left.mCount + right.mCount > 0 );
	
	int leftId = 0, rightId = 0;

	for( int i=0; i<in.mCount; ++i )
	{
		int leftTC = 0, rightTC = 0;
		splitTriangle< axis >( in(i), leftTC, rightTC, orthValue );
		
		if ( leftTC > 0 )
		{
			*(left[leftId]) = *(in[i]);
			++leftId;
		}

		if ( rightTC > 0 )
		{
			*(right[rightId]) = *(in[i]);
			++rightId;
		}	
	}

	Assert( left.mCount == leftId && right.mCount == rightId );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BVHNode*
BVHNode::splitNodeByMedian( const BTVec& trigs, BTVec& trigsLeft, BTVec& trigsRight )
{
	double x = 0, y = 0, z = 0;
	
	const int count = trigs.mCount;
	int currentCount = count;

	for( int i=0; i<count; ++i)
	{
		x += trigs(i).box.center.x;
		y += trigs(i).box.center.y;
		z += trigs(i).box.center.z;
	}

	x/=count;
	y/=count;
	z/=count;

	int i = 0;
	int step = 20;
	
	AABB bounds = getAABB(trigs);

	double xBestSAH;
	double yBestSAH;
	double zBestSAH;

	int   xLeftCount, xRightCount;
	int   yLeftCount, yRightCount;
	int   zLeftCount, zRightCount;

	xBestSAH = getSAH< BVHNode::x >( trigs, x, xLeftCount, xRightCount );
	yBestSAH = getSAH< BVHNode::y >( trigs, y, yLeftCount, yRightCount );
	zBestSAH = getSAH< BVHNode::z >( trigs, z, zLeftCount, zRightCount );

	if ( xBestSAH <= yBestSAH && xBestSAH <= zBestSAH )
	{
		trigsLeft.alloc( xLeftCount );
		trigsRight.alloc( xRightCount );
		splitByAxis< BVHNode::x >( trigs, trigsLeft, trigsRight, x );
	}
	else if ( yBestSAH <= xBestSAH && yBestSAH <= zBestSAH )
	{
		trigsLeft.alloc( yLeftCount );
		trigsRight.alloc( yRightCount );
		splitByAxis< BVHNode::y >( trigs, trigsLeft, trigsRight, y );
	}
	else
	{
		trigsLeft.alloc( zLeftCount );
		trigsRight.alloc( zRightCount );
		splitByAxis< BVHNode::z >( trigs, trigsLeft, trigsRight, z );
	}

	
	BVHNode * result = (BVHNode*) malloc( sizeof (BVHNode) );
	BVHNode::build(	*result,  bounds );
	return result;
}



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


inline void
stopSplit( const int left, const int right, const int total, bool& stopLeft, bool& stopRight  )
{
	const float ratioR = left / float(total);
	const float ratioL = right / float(total);
		
	if ( left < STOP_SPLIT_TRIGS_PER_NODE_COUNT || right < STOP_SPLIT_TRIGS_PER_NODE_COUNT || right == total || left == total )
	{
		stopLeft = true;
		stopRight = true;
		return;
	}

	stopLeft = false;
	stopRight = false;

	if ( ratioL > STOP_FIRST_K )
	{
		if ( ratioR > STOP_SECOND_K )
			stopLeft = true;
	}	

	if ( ratioR > STOP_FIRST_K )
	{
		if ( ratioL > STOP_SECOND_K )
			stopRight = true;
	}	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < BVHNode::Axis axis, AABB::Enum maxMin >
inline void
AssertSorted( const BoundedTriangle** array, const int count, const bool upRise )
{
#ifdef _DEBUG 
	for( int i=0; i<count-1; ++i )
	{		
		float first = array[i]->box.min()[axis];
		float second = array[i]->box.max()[axis];

		if ( upRise )
			Assert( first <= second );
		else
			Assert( first >= second );
	}
#endif // _DEBUG
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

template < BVHNode::Axis axis >
inline void splitTriangle( const BoundedTriangle& trig, int& leftCount, int& rightCount, const float orthValue )
{
	const float min = trig.box.min()[axis];
	const float max = trig.box.max()[axis];

	rightCount += int ( min >= orthValue || max > orthValue );
	leftCount  += int ( min < orthValue );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < BVHNode::Axis axis >
inline double getBestSplitSAH(
		const BTVec& vecSortedByMaxBound
	,	const BTVec& vecSortedByMinBound
	,	float& resultOrth
	,	int& leftCount
	,	int& rightCount
	,	float lowerBound
	,	float upperBound
)
{
	float WorkingOrthAABBLength = vecSortedByMaxBound.top().box.max()[axis] - vecSortedByMinBound.bot().box.min()[axis];
	Assert( WorkingOrthAABBLength >=0 );

	leftCount = 0;
	rightCount = 0;
	int maxCount = vecSortedByMinBound.mCount;
	Assert( vecSortedByMinBound.mCount == vecSortedByMaxBound.mCount && maxCount > 1 );

	if ( upperBound == lowerBound )
	{
		resultOrth = lowerBound;
		leftCount = vecSortedByMaxBound.mCount;
		rightCount = leftCount;
		return MAX_SAH;
	}
	
	const int maxSplitDecreaseCount = MAX_SPLIT_DECREASE_COUNT;
	const int originId = maxCount / 2;

	int currDicreaseSAHCount = 0;	

	float resultBestSplitOrth = vecSortedByMinBound(originId).box.min()[axis];
	
	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	double SAH = MAX_SAH;

	for( int i=originId; i>0; --i )
	{
		if( currDicreaseSAHCount > maxSplitDecreaseCount )
			break;

		int lc,rc;
		const float minBoundSplit = vecSortedByMinBound(i).box.min()[axis];
		if ( ! inSegment( lowerBound, upperBound, minBoundSplit ) )
		{
			++currDicreaseSAHCount;
			continue;
		}
		
		const double newSAH = getSAH< axis >( vecSortedByMinBound, minBoundSplit, lc, rc );
		if ( newSAH < SAH )
		{
			leftCount = lc;
			rightCount = rc;
			SAH = newSAH;
			resultBestSplitOrth = minBoundSplit;
			currDicreaseSAHCount = 0;
		}
		else
			++currDicreaseSAHCount;
	}


	currDicreaseSAHCount = 0;
	for( int i=originId; i<maxCount-1; ++i )
	{
		if( currDicreaseSAHCount > maxSplitDecreaseCount )
			break;

		int lc,rc;
		const float maxBoundSplit = vecSortedByMaxBound(i).box.max()[axis];
		if ( ! inSegment( lowerBound, upperBound, maxBoundSplit ) )
		{
			++currDicreaseSAHCount;
			continue;
		}

		const double newSAH = getSAH< axis >( vecSortedByMaxBound, maxBoundSplit, lc, rc );
		if ( newSAH < SAH  )
		{
			leftCount = lc;
			rightCount = rc;
			SAH = newSAH;
			resultBestSplitOrth = maxBoundSplit;
			currDicreaseSAHCount = 0;
		}
		else
			++currDicreaseSAHCount;
	}


	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


 	float tempMedianOrth = vecSortedByMinBound(originId).box.center[axis];
 	const float medianStep = WorkingOrthAABBLength / 128.f;
 
//    	currDicreaseSAHCount = 0;
//    	for( int i=0; i<64; ++i )
//    	{
//   		if( currDicreaseSAHCount > maxSplitDecreaseCount )
//   			break;
//   
//   		if ( ! inSegment( lowerBound, upperBound, tempMedianOrth ) )
//   		{
//   			++currDicreaseSAHCount;
//    			continue;
//    		}
//   
//   		int lc,rc;		
//   		const double newSAH = getSAH< axis >( vecMinBound, tempMedianOrth, lc, rc );
//    		if ( newSAH < SAH )
//    		{
//    			leftCount = lc;
//    			rightCount = rc;
//    			SAH = newSAH;
//    			resultBestSplitOrth = tempMedianOrth;
//    			currDicreaseSAHCount = 0;
//    		}
//    		else
//    			++currDicreaseSAHCount;
//    		
//    		tempMedianOrth += medianStep;
//    	}
//    	
//    	tempMedianOrth = vecMaxBound(originId).box.center[axis];
//    	currDicreaseSAHCount = 0;
//    	for( int i=0; i<64; ++i )
//    	{
//   		if( currDicreaseSAHCount > maxSplitDecreaseCount )
//   			break;
//   
//   		if ( ! inSegment( lowerBound, upperBound, tempMedianOrth ) )
//   		{
//   			++currDicreaseSAHCount;
//   			continue;
//   		}
//   
//    		int lc,rc;		
//    		const double newSAH = getSAH< axis >( vecMaxBound, tempMedianOrth, lc, rc );
//    		if ( newSAH < SAH )
//    		{
//    			leftCount = lc;
//    			rightCount = rc;
//    			SAH = newSAH;
//    			resultBestSplitOrth = tempMedianOrth;
//    			currDicreaseSAHCount = 0;
//    		}
//    		else
//    			++currDicreaseSAHCount;
//    	
//    		tempMedianOrth -= medianStep;
//    	}

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	tempMedianOrth = (lowerBound + upperBound ) * 0.5f;
	currDicreaseSAHCount = 0;
	for( int i=0; i<1; ++i )
	{
		if( currDicreaseSAHCount > maxSplitDecreaseCount )
			break;

		if ( ! inSegment( lowerBound, upperBound, tempMedianOrth ) )
		{
			++currDicreaseSAHCount;
			continue;
		}

		int lc,rc;		
		const double newSAH = getSAH< axis >( vecSortedByMaxBound, tempMedianOrth, lc, rc );
		if ( newSAH < SAH )
		{
			leftCount = lc;
			rightCount = rc;
			SAH = newSAH;
			resultBestSplitOrth = tempMedianOrth;
			currDicreaseSAHCount = 0;
		}
		else
			++currDicreaseSAHCount;

		tempMedianOrth -= medianStep;
	}

	tempMedianOrth = (lowerBound + upperBound ) * 0.5f;
	currDicreaseSAHCount = 0;
	for( int i=0; i<1; ++i )
	{
		if( currDicreaseSAHCount > maxSplitDecreaseCount )
			break;

		if ( ! inSegment( lowerBound, upperBound, tempMedianOrth ) )
		{
			++currDicreaseSAHCount;
			continue;
		}

		int lc,rc;		
		const double newSAH = getSAH< axis >( vecSortedByMinBound, tempMedianOrth, lc, rc );
		if ( newSAH < SAH )
		{
			leftCount = lc;
			rightCount = rc;
			SAH = newSAH;
			resultBestSplitOrth = tempMedianOrth;
			currDicreaseSAHCount = 0;
		}
		else
			++currDicreaseSAHCount;

		tempMedianOrth += medianStep;
	}

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	resultOrth = resultBestSplitOrth;
	Assert( SAH > 0 );
	return SAH;

} // getBestSplitSAH


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < BVHNode::Axis axis, AABB::Enum maxMin >
inline void
quickSortTriangleByBounds( const BoundedTriangle** array, const int first, const int last )
{
	int i = first, j = last;
	const BoundedTriangle* x = array[ ( first + last ) / 2 ];
	float valueXMaxMin = x->box.bounds[maxMin][axis];

	do {
		while ( i <= last && array[i]->box.bounds[maxMin][axis] < valueXMaxMin )
			i++;

		while ( j >= 0 && array[j]->box.bounds[maxMin][axis] > valueXMaxMin )
			j--;

		if(i <= j)
		{
			if (i < j) 
			{
				const BoundedTriangle* temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}

			i++;
			j--;
		}

	} while (i <= j && i<= last && j>=0 );


	if (i < last)
		quickSortTriangleByBounds< axis, maxMin >( array, i, last );
	if (first < j)
		quickSortTriangleByBounds< axis, maxMin >( array, first, j );

} // quickSortTriangleByBounds


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void BVHNode::build( BVHNode& node, const Vec3& minBound, const Vec3& maxBound )	
{
	node.mBounds.set( minBound, maxBound );
	node.mTrigs.setZero();
	node.mLeft = 0;
	node.mRight = 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void BVHNode::build( BVHNode& node, const AABB& box )
{
	node.mBounds = box;
	node.mTrigs.setZero();
	node.mLeft = 0;
	node.mRight = 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BVHNode *
BVHNode::buildBVHLeaf( BTVec& trigs )
{
	if ( trigs.mCount < 1 )
		return 0;
	
	BVHNode * result = ( BVHNode* ) malloc( sizeof (BVHNode) );
	build( *result, getAABB( trigs ) );

	result->mTrigs.swap( trigs );
	return result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BVHNode*
BVHNode::buildBVHNode( const BTVec& trigs, int currentDepth )
{	
	++currentDepth;
	if ( trigs.mCount <= 0 )
		return 0;	

	BTVec left, right;
	bool stopLeft, stopRight;	
	BVHNode * result = 0;

	if ( trigs.mCount < 40 )
	{
		BTVec xTriMin, xTriMax;		xTriMin.copy( trigs );	xTriMax.copy( trigs );
		BTVec yTriMin, yTriMax;		yTriMin.copy( trigs );	yTriMax.copy( trigs );
		BTVec zTriMin, zTriMax;		zTriMin.copy( trigs );	zTriMax.copy( trigs );
	
		int totalCount = xTriMin.mCount;
		int lastTrigId = totalCount - 1;

		if ( xTriMin.mCount > 10 )
		{
			#pragma omp parallel sections
			{
				#pragma  omp section
				{
					quickSortTriangleByBounds< BVHNode::x, AABB::Min >( xTriMin.mTrigs, 0, lastTrigId );
					AssertSorted< BVHNode::x, AABB::Min >(xTriMin.mTrigs, xTriMin.mCount, true );
			
					quickSortTriangleByBounds< BVHNode::x, AABB::Max >( xTriMax.mTrigs, 0, lastTrigId );
					AssertSorted< BVHNode::x, AABB::Max >(xTriMax.mTrigs, xTriMax.mCount, true );
				}
				#pragma  omp section
				{
					quickSortTriangleByBounds< BVHNode::y, AABB::Min >( yTriMin.mTrigs, 0, lastTrigId );
					AssertSorted< BVHNode::y, AABB::Min >(yTriMin.mTrigs, yTriMin.mCount, true );
		
					quickSortTriangleByBounds< BVHNode::y, AABB::Max >( yTriMax.mTrigs, 0, lastTrigId );
					AssertSorted< BVHNode::y, AABB::Max >(yTriMax.mTrigs, yTriMax.mCount, true );
				}	
				#pragma  omp section
				{
					quickSortTriangleByBounds< BVHNode::z, AABB::Min >( zTriMin.mTrigs, 0, lastTrigId );
					AssertSorted< BVHNode::z, AABB::Min >(zTriMin.mTrigs, zTriMin.mCount, true );
			
					quickSortTriangleByBounds< BVHNode::z, AABB::Max >( zTriMax.mTrigs, 0, lastTrigId );
					AssertSorted< BVHNode::z, AABB::Max >(zTriMax.mTrigs, zTriMax.mCount, true );
				}
	
			} // OMP-parallel sort
		}
		else
		{
			quickSortTriangleByBounds< BVHNode::x, AABB::Min >( xTriMin.mTrigs, 0, lastTrigId );
			AssertSorted< BVHNode::x, AABB::Min >(xTriMin.mTrigs, xTriMin.mCount, true );

			quickSortTriangleByBounds< BVHNode::x, AABB::Max >( xTriMax.mTrigs, 0, lastTrigId );
			AssertSorted< BVHNode::x, AABB::Max >(xTriMax.mTrigs, xTriMax.mCount, true );

			quickSortTriangleByBounds< BVHNode::y, AABB::Min >( yTriMin.mTrigs, 0, lastTrigId );
			AssertSorted< BVHNode::y, AABB::Min >(yTriMin.mTrigs, yTriMin.mCount, true );

			quickSortTriangleByBounds< BVHNode::y, AABB::Max >( yTriMax.mTrigs, 0, lastTrigId );
			AssertSorted< BVHNode::y, AABB::Max >(yTriMax.mTrigs, yTriMax.mCount, true );

			quickSortTriangleByBounds< BVHNode::z, AABB::Min >( zTriMin.mTrigs, 0, lastTrigId );
			AssertSorted< BVHNode::z, AABB::Min >(zTriMin.mTrigs, zTriMin.mCount, true );

			quickSortTriangleByBounds< BVHNode::z, AABB::Max >( zTriMax.mTrigs, 0, lastTrigId );
			AssertSorted< BVHNode::z, AABB::Max >(zTriMax.mTrigs, zTriMax.mCount, true );

		} // non-parallel sort


		float xSplitOrth;	double xBestSAH;
		float ySplitOrth;	double yBestSAH;
		float zSplitOrth;	double zBestSAH;
	
		int   xLeftCount, xRightCount;
		int   yLeftCount, yRightCount;
		int   zLeftCount, zRightCount;

		result = (BVHNode*) malloc( sizeof (BVHNode) );
		build(
				*result
			,	Vec3( xTriMin.bot().box.min().x, yTriMin.bot().box.min().y, zTriMin.bot().box.min().z )
			,	Vec3( xTriMax.top().box.max().x, yTriMax.top().box.max().y, zTriMax.top().box.max().z )
		);

		if ( currentDepth >= MAX_DEPTH )
		{
			result->mTrigs.swap( xTriMin );
			return result;
		}

		Assert ( currentDepth < MAX_DEPTH );

		//#pragma omp parallel sections
		{
			//#pragma omp section
			{
				xBestSAH = getBestSplitSAH< BVHNode::x >( xTriMin, xTriMax, xSplitOrth, xLeftCount, xRightCount, result->mBounds.min().x, result->mBounds.max().x );

	#ifdef _DEBUG
				Assert( xBestSAH > 0 );
				if ( xBestSAH < MAX_SAH )
				{
					Assert( xSplitOrth >= result->mBounds.min().x );
					Assert( xSplitOrth <= result->mBounds.max().x );
				}
	#endif // DEBUG

			}
			//#pragma omp section
			{
				yBestSAH = getBestSplitSAH< BVHNode::y >( yTriMin, yTriMax, ySplitOrth, yLeftCount, yRightCount, result->mBounds.min().y, result->mBounds.max().y );

	#ifdef _DEBUG
				Assert( yBestSAH > 0 );
				if ( yBestSAH < MAX_SAH )
				{
					Assert( ySplitOrth >= result->mBounds.min().y );
					Assert( ySplitOrth <= result->mBounds.max().y );
				}
	#endif // DEBUG
	
			}
			//#pragma omp section
			{
				zBestSAH = getBestSplitSAH< BVHNode::z >( zTriMin, zTriMax, zSplitOrth, zLeftCount, zRightCount, result->mBounds.min().z, result->mBounds.max().z );

	#ifdef _DEBUG
				Assert( zBestSAH > 0 );
				if ( zBestSAH < MAX_SAH )
				{
					Assert( zSplitOrth >= result->mBounds.min().z );
					Assert( zSplitOrth <= result->mBounds.max().z );
				}
	#endif // DEBUG

			}

		} // BIG SAH build
	


		//http://ray-tracing.ru/articles184.html
		//object split is slow-traced, spatial more effective!

		if ( xBestSAH < MAX_SAH && xBestSAH <= yBestSAH && xBestSAH <= zBestSAH )
		{
			if ( currentDepth > MAX_DEPTH - 10 )
			{
				int a = 0;
			}
			stopSplit( xLeftCount, xRightCount, totalCount, stopLeft, stopRight);

			left.alloc( xLeftCount );
			right.alloc( xRightCount );
			splitByAxis< BVHNode::x >( xTriMin, left, right, xSplitOrth );
		}	
		else if ( yBestSAH < MAX_SAH && yBestSAH <= xBestSAH && yBestSAH <= zBestSAH )
		{
			stopSplit( yLeftCount, yRightCount, totalCount, stopLeft, stopRight );

			left.alloc( yLeftCount );
			right.alloc( yRightCount );
			splitByAxis< BVHNode::y >( yTriMin, left, right, ySplitOrth );
		}
		else if ( zBestSAH < MAX_SAH )
		{
			stopSplit( zLeftCount, zRightCount, totalCount, stopLeft, stopRight );

			left.alloc( zLeftCount );
			right.alloc( zRightCount );
			splitByAxis< BVHNode::z >( zTriMin, left, right, zSplitOrth );
		}
		

		if (	( xBestSAH >= MAX_SAH && yBestSAH >= MAX_SAH && zBestSAH >= MAX_SAH )
			||	( stopRight && stopLeft )
		)
		{
			result->mTrigs.swap( zTriMin );
			return result;
		}


		xTriMin.die();	yTriMin.die();	zTriMax.die();
		xTriMax.die();	yTriMax.die();	zTriMin.die();
		}
	else
	{
		result = splitNodeByMedian( trigs, left, right );
		stopSplit( left.mCount, right.mCount, trigs.mCount, stopLeft, stopRight );
	}


	if ( stopLeft || stopRight || left.mCount < STOP_SPLIT_TRIGS_PER_NODE_COUNT || right.mCount < STOP_SPLIT_TRIGS_PER_NODE_COUNT )
	{
		if ( stopLeft || left.mCount < STOP_SPLIT_TRIGS_PER_NODE_COUNT )
			result->mLeft = BVHNode::buildBVHLeaf( left );
		else
			result->mLeft = BVHNode::buildBVHNode( left, currentDepth );

		if ( stopRight || right.mCount < STOP_SPLIT_TRIGS_PER_NODE_COUNT )
			result->mRight = BVHNode::buildBVHLeaf( right );
		else
			result->mRight = BVHNode::buildBVHNode( right, currentDepth );
	}
	else
	{
		#pragma omp parallel sections
		{
			#pragma omp section
			{
				if ( stopLeft || left.mCount < STOP_SPLIT_TRIGS_PER_NODE_COUNT )
					result->mLeft = BVHNode::buildBVHLeaf( left );
				else
					result->mLeft = BVHNode::buildBVHNode( left, currentDepth );
			}
		
			#pragma omp section
			{
				if ( stopRight || right.mCount < STOP_SPLIT_TRIGS_PER_NODE_COUNT )
					result->mRight = BVHNode::buildBVHLeaf( right );
				else
					result->mRight = BVHNode::buildBVHNode( right, currentDepth );
			}
		}
	}

	return result;

} // BVHNode::buildBVHTree


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ void
BVHNode::traceNearest( const Ray& ray, Intersection& out, float maxQuadDist, const BoundedTriangle* ignore )
{
	float isecPointOriginDist;
	if ( !Intersection::intersect( ray, mBounds, isecPointOriginDist ) )
	{
		out.resetType();
		return;
	}

#ifdef _DEBUG

	if ( 0 == mRight && mLeft == 0 )
		Assert( mTrigs.mCount > 0 );

	if ( 0 != mRight || mLeft != 0 )
		Assert( mTrigs.mCount == 0 );

#endif // _DEBUG

	Intersection tempIsec;
	if ( mTrigs.mCount > 0 )
	{
		traceBrutalNearestTrigs( tempIsec, ray, mTrigs, maxQuadDist, ignore );
		out.setBestIsec( maxQuadDist, tempIsec );

		Assert( 0 == mRight && mLeft == 0 );
		return;
	}
	
	float distQLeft = MAX_SAH;
	float distQRight = MAX_SAH;
	volatile bool traceLeft = false;
	volatile bool traceRight = false;

	if ( mLeft )
		if ( Intersection::intersect( ray, mLeft->mBounds, distQLeft ) )
			traceLeft = true;
	
	if ( mRight )
		if ( Intersection::intersect( ray, mRight->mBounds, distQRight ) )
			traceRight = true;

	BVHNode* first, * second;
	if ( distQLeft > distQRight )
	{
		first = mRight;
		second = mLeft;
	}
	else
	{
		first = mLeft;
		second = mRight;
	}
	if ( ! (traceRight || traceLeft ) )
		return;

	if( first )
	{
		tempIsec.resetType();
		first->traceNearest( ray, tempIsec, maxQuadDist, ignore );
		out.setBestIsec( maxQuadDist, tempIsec );
// 
// 		if ( out.hasOnePointContact() )
// 			return;
	}
		
	if ( second )
	{
// 		if ( out.mQuadDistance < distQRight || out.mQuadDistance < distQLeft )
// 			return;

		tempIsec.resetType();
		second->traceNearest( ray, tempIsec, maxQuadDist, ignore );
		out.setBestIsec( maxQuadDist, tempIsec );
	}
	
} // BVHNode::traceNearest


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ bool
BVHNode::traceAny( const Ray& ray, float maxQuadDist, const BoundedTriangle* ignore )
{
	float isecPointOriginDist;
	if ( !Intersection::intersect( ray, mBounds, isecPointOriginDist ) )
		return false;

#ifdef _DEBUG

	if ( 0 == mRight && mLeft == 0 )
		Assert( mTrigs.mCount > 0 );

	if ( 0 != mRight || mLeft != 0 )
		Assert( mTrigs.mCount == 0 );

#endif // _DEBUG

	if ( traceBrutalAnyTrigs( ray, mTrigs, maxQuadDist, ignore ) )
		return true;

	float distQLeft = MAX_SAH;
	float distQRight = MAX_SAH;
	bool traceLeft = false;
	bool traceRight = false;

	if ( mLeft )
		if ( Intersection::intersect( ray, mLeft->mBounds, distQLeft ) )
			traceLeft = true;

	if ( mRight )
		if ( Intersection::intersect( ray, mRight->mBounds, distQRight ) )
			traceRight = true;

	BVHNode* first = 0;
	BVHNode* second = 0;

	if ( distQLeft < distQRight )
	{
		if ( traceLeft )
			first = mLeft;
		if ( traceRight )
			second = mRight;
	}
	else
	{
		if ( traceRight )
			first = mRight;
		if ( traceLeft )
			second = mLeft;
	}
	
	if ( first )
	{
		if ( first->traceAny( ray, maxQuadDist, ignore ) )
			return true;
	}

	if ( second )
	{
		if ( second->traceAny( ray, maxQuadDist, ignore ) )
			return true;
	}

	return false;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ bool
BVHNode::traceBrutalAnyTrigs ( const Ray& ray, const BTVec& vec, float maxQuadDist, const BoundedTriangle* ignore )
{
	int count = vec.mCount;
	for( int i=0; i<count; ++i )
	{
		if ( ignore == &vec(i) )
			continue;

		Intersection isec;
		Intersection::intersect( isec, ray, vec(i) );
				
		if ( isec.hasOnePointContact() && maxQuadDist >= isec.mQuadDistance )
			return true;

	}

	return false;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ void
BVHNode::traceBrutalNearestTrigs( Intersection& out, const Ray& ray, const BTVec& vec, float maxQuadDist, const BoundedTriangle* ignore )
{
	int count = vec.mCount;

	if ( count < 1 )
		return;

	Intersection temp;

	for( int i=0; i<count; ++i )
	{
		if ( &vec(i) == ignore )
			continue;
		
		Intersection::intersect( temp, ray, vec(i) );

		if ( temp.hasOnePointContact() )
		{
			if (	maxQuadDist >= temp.mQuadDistance
				&&	( ! out.hasOnePointContact() || out.mQuadDistance > temp.mQuadDistance )
			)
			{
				out = temp;
			}
		}
	}

} // BVHNode::traceTrigs


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BVHDiagnostic
BVHDiagnostic::traverse( const BVHNode& root, int trigsTotalCount )
{
	BVHDiagnostic result;
	result.visit( root, 0 );
		
	if( result.detectedTrigs.size() != trigsTotalCount )
	{
		if ( result.detectedTrigs.size() < trigsTotalCount )
			throw std::exception("lost trigs in BVH!");
		else
			throw std::exception("excessive trigs in BVH!");		
	}

	return result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
BVHDiagnostic::visit( const BVHNode& node, int depth )
{
	++depth;
	maxDepth = maximal( depth, maxDepth );

	++nodesCount;
	trigsPtrCount += node.mTrigs.mCount;
	
	for( int i=0;i<node.mTrigs.mCount; ++i )
	{
		detectedTrigs.insert( node.mTrigs.mTrigs[i] );
	}

	maxTrigsPerNode = maximal( maxTrigsPerNode, node.mTrigs.mCount );

	if ( node.mTrigs.mCount < maxCount )
		trigsPerNodeCount[node.mTrigs.mCount] += 1;
	else
		++extraTrigsNodesCount;
		
	if ( node.mLeft )
		visit( *node.mLeft, depth );

	if ( node.mRight )
		visit( *node.mRight, depth );

	if ( node.mLeft == node.mRight && node.mTrigs.mCount <= 0 )
	{
		throw std::exception("uncorrect depth of BVH");
	}

	if ( (!node.mLeft && node.mRight) || ( node.mLeft && !node.mRight ) )
	{
		throw std::exception("imbalanced node found in BVH");
	}
	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BVHPacked*
BVHNode::buildPackedBVH( BTVec& trigs )
{
	BVHNode* rawTree = buildBVHNode( trigs, 0 );
	BVHPacked* packed = new BVHPacked();
	packed->visitGatherData( *rawTree );
	
	packed->mTrigs.alloc( packed->mTrigs.mCount );
	packed->mBVHNodes = (BVHNode*) malloc( sizeof(BVHNode) * packed->mBVHNodesCount );

	packed->mCurrentBuildingBVHNode = packed->mBVHNodes;
	packed->mCurrentBuildingTrigPtr = packed->mTrigs.mTrigs;

	packed->visitCopyData( *rawTree );
	packed->killNotPacked( *rawTree );

	return packed;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
BVHPacked::visitGatherData( BVHNode& node )
{
	mBVHNodesCount++;
	mTrigs.mCount += node.mTrigs.mCount;

	if ( node.mLeft )
		visitGatherData( *node.mLeft );
	if ( node.mRight )
		visitGatherData( *node.mRight );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


BVHNode*
BVHPacked::visitCopyData( const BVHNode& nodeSource )
{
	BVHNode* buildNode = mCurrentBuildingBVHNode;
	buildNode->mTrigs.mCount = nodeSource.mTrigs.mCount;
	
	buildNode->mBounds = nodeSource.mBounds;

	Assert( nodeSource.mTrigs.mCount >= 0 );
	if ( nodeSource.mTrigs.mCount > 0 )
	{
		memcpy( mCurrentBuildingTrigPtr, nodeSource.mTrigs.mTrigs, sizeof(BoundedTriangle**) * nodeSource.mTrigs.mCount );
		buildNode->mTrigs.mTrigs = mCurrentBuildingTrigPtr;
		mCurrentBuildingTrigPtr += nodeSource.mTrigs.mCount;

		Assert( nodeSource.mLeft == 0 && nodeSource.mRight == 0 );
		// only leafs have trigs
		buildNode->mLeft = 0;
		buildNode->mRight = 0;
		return buildNode;
	}
	else
		buildNode->mTrigs.mTrigs = 0;

	if ( nodeSource.mLeft )
	{
		++mCurrentBuildingBVHNode;
		buildNode->mLeft = visitCopyData( *nodeSource.mLeft );
	}
	else
		buildNode->mLeft = 0;


	if ( nodeSource.mRight )
	{
		++mCurrentBuildingBVHNode;
		buildNode->mRight = visitCopyData( *nodeSource.mRight );
	}
	else
		buildNode->mRight = 0;

	Assert( buildNode->mTrigs.mCount >=0 && buildNode->mTrigs.mCount < 10 );

	return buildNode;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
BVHPacked::killNotPacked( BVHNode& node )
{
	node.mTrigs.die();

	if( node.mLeft )
		killNotPacked( *node.mLeft );
	if( node.mRight )
		killNotPacked( *node.mRight );

	free(&node);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
