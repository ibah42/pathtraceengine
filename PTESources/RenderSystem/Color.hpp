#pragma once

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// TRUE color - doesn't use alpha!
class Color
{
public:	

	Color()
	{}

	Color( float _r, float _g, float _b )
	{
		r = _r;
		g = _g;
		b = _b;
	}

	void set( float _r, float _g, float _b )
	{
		r = _r;
		g = _g;
		b = _b;
	}

// IF this realy need - do not uncomment just, create static builder-function!
// if this is ignored - incorrect color( 8-bit char ) could be crated in shaders, materials etc!
// 	Color( int _r, int _g, int _b )
// 		:	r( _r / 255.f )
// 		,	g( _g / 255.f )
// 		,	b( _b / 255.f )
// 	{}


	void operator += ( const Color& c )
	{
		r += c.r;
		g += c.g;
		b += c.b;
	}
	
	Color operator += ( const Color& c ) const
	{
		return Color( r+c.r, g+c.g, b+c.b );
	}

	Color operator - ( const Color& c )
	{
		return Color( r - c.r, g - c.g, b - c.b );
	}

	float derivationR ( const Color& c )
	{
		return fabs( r - c.r );
	}

	float derivationG ( const Color& c )
	{
		return fabs( g - c.g );
	}

	float derivationB ( const Color& c )
	{
		return fabs( b - c.b );
	}

	void operator /= ( float f )
	{
		float k = 1.f / f;
		r *= k;
		g *= k;
		b *= k;
	}

	Color operator / ( float c ) const
	{
		float k = 1 / c;
		return Color ( r*k, g*k, b*k );
	}

	void operator *= ( float f )
	{
		r *= f;
		g *= f;
		b *= f;
	}

	Color operator * ( const Color& c ) const
	{
		return Color( r*c.r, g*c.g, b*c.b );		
	}

	Color operator * ( float f ) const
	{
		return Color( r*f, g*f, b*f );
	}

public:

	float r, g, b;

}; // class Color


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct ColorA
{
	ColorA(){}

	ColorA( const Color& c, float a )
		:	c(c)
		,	alpha(a)
	{}

	ColorA( float r, float g, float b, float a )
	{
		c.r = r;
		c.g = g;
		c.b = b;
		alpha = a;
	}

	ColorA operator *( float intescity )
	{
		ColorA r = *this;
		r.c *= intescity;
		return r;
	}

public:

	Color c;
	float alpha;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
