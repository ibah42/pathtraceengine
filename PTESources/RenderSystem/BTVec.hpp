#pragma once

#include "PTESources/Math/Geometry.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct BTVec
{
	void setZero()
	{
		mTrigs = 0;
		mCount = 0;
	}

	const BoundedTriangle& operator() ( int id ) const;

	const BoundedTriangle** operator[] ( int id ) const;

	const BoundedTriangle& top() const;

	const BoundedTriangle& bot() const;
	
	void swap( BTVec& that );

	static BTVec buildFromArray( const BoundedTriangle* array, int count );

	BTVec( BTVec& that );
	BTVec();
	BTVec( int count, const BoundedTriangle** trigs );
	BTVec( int count );

	void copy( const BTVec& that );
	void copy( const BoundedTriangle** trigs );

	void alloc( int count );
	void die();
	~BTVec();

private:
public:

	const BoundedTriangle** mTrigs;
	 int mCount;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
