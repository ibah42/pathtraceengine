#pragma once

#include "PTESources/RenderSystem/Color.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct ColorTexture
{

	ColorTexture( const ColorA& c )
	{
		mx = 1;
		my = 1;
		buffer = new Texel(c);
	}

	~ ColorTexture()
	{
		delete buffer;
	}

	ColorTexture( const char* fileName );

	struct Texel
	{
		Texel(){}

		Texel( const ColorA& c )
		{
			al =(Byte)clamp( 0, 0xFF, int(c.alpha * 0xFF) );
			r = (Byte)clamp( 0, 0xFF, int(c.c.r * 0xFF) );
			g = (Byte)clamp( 0, 0xFF, int(c.c.g * 0xFF) );
			b = (Byte)clamp( 0, 0xFF, int(c.c.b * 0xFF) );
		}

		Texel( const Color& c )
		{
			al = 0xff;
			r = (Byte)clamp( 0, 0xFF, int(c.r * 0xFF) );
			g = (Byte)clamp( 0, 0xFF, int(c.g * 0xFF) );
			b = (Byte)clamp( 0, 0xFF, int(c.b * 0xFF) );
		}

	public:

		Byte r, g, b, al;
	};

	Texel operator() ( int x, int y )
	{
		Assert( mx*y + mx < mx*my );
		return buffer [ mx*y + mx ];
	}


	ColorA onFilterBiLinear( float u, float v ) const;


public:

	Texel* buffer;

	int totalCount;
	int mx;
	int my;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct EmitTexture
	:	public ColorTexture
{
	EmitTexture( const ColorA& c, float intensity )
		:	ColorTexture( c )
		,	intensity(intensity)
	{}

	EmitTexture( const char* fileName, float intensity )
		:	ColorTexture( fileName )
		,	intensity(intensity)
	{}
	
	ColorA onFilterBiLinear( float u, float v ) const
	{
		return ColorTexture::onFilterBiLinear( u, v ) * intensity;
	}

public:

	float intensity;

}; // struct EmitTexture


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct NormalTexture
{

	NormalTexture()
	{
		mx = 0;
		my = 0;
		buffer = 0;
	}

	~ NormalTexture()
	{
		delete buffer;
	}

	NormalTexture( const char* fileName );
	
	Vec3 onFilterBiLinear( float u, float v, const Vec3& trueNormal ) const;
	
public:

	Vec3* buffer;

	int totalCount;
	int mx;
	int my;

}; // struct NormalTexture



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct Spheremap
{
	Color intersect ( const Ray& ray );

	Spheremap( const char * fileName );

private:

	ColorTexture tex;

}; // struct Spheremap



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
