#include "PTESources/PCH.hpp"
#include "PTESources/RenderSystem/RenderBuffer.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
RenderBufferStack::resize( int layers, int x, int y )
{
	Assert( x * y * layers > 0 );
		
	if ( layers != mLayers || mNbPixelsX != x || mNbPixelsY != y )
	{
		free( mCommonColorBuff );
		free( mCommonSquareDepthBuff );
		
		if ( mRenderBuffers)
		{
			free( mRenderBuffers );
		}

		mLayers = layers;
		mRenderBuffers = static_cast< RenderBuffer* >( malloc( sizeof( RenderBuffer ) * layers ) );

		mNbPixelsX = x;
		mNbPixelsY = y;
		alloc();

		for( int i=0; i<layers; ++i )
			mRenderBuffers[i].resize( mNbPixelsX, mNbPixelsY, getColorSlice( i ), getSquareDepthSlice( i ) );

		return true;
	}

	return false;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
RenderBufferStack::swapBuffers( int layer1, int layer2 )
{		
	RenderBuffer& rb1 = mRenderBuffers[ layer1 ];
	RenderBuffer& rb2 = mRenderBuffers[ layer2 ];

	Color* tempColor = rb1.mColorBuff;
	float* tempDepth = rb1.mSquareDepthBuff;
	int x = rb1.mNbPixelsX;
	int y = rb1.mNbPixelsY;

	rb1.mSquareDepthBuff = rb2.mSquareDepthBuff;
	rb1.mColorBuff = rb2.mColorBuff;
	rb1.mNbPixelsX = rb2.mNbPixelsX;
	rb1.mNbPixelsY = rb2.mNbPixelsY;

	rb2.mSquareDepthBuff = tempDepth;
	rb2.mColorBuff = tempColor;
	rb2.mNbPixelsX = x;
	rb2.mNbPixelsY = y;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void RenderBufferStack::alloc()
{
	mCommonColorBuff = static_cast< Color* >(
		malloc( sizeof( Color ) * mNbPixelsX * mNbPixelsY * mLayers )
	);

	mCommonSquareDepthBuff = static_cast< float* >(
		malloc( sizeof( float ) * mNbPixelsX * mNbPixelsY * mLayers )
	);
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
