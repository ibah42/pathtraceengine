#include "PTESources/PCH.hpp"
#include "PTESources/RenderSystem/Shaders.hpp"
#include "PTESources/RenderSystem/Scene.hpp"
#include "PTESources/RenderSystem/Texture.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define ALMOST_HALF_PI (Pi/2 - 0.001f)

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Color
SurfaceShader::onGeometry( const VertexMaterial* prevVertexMat,const Intersection& sourceIsec, const Ray& inRay, const RayTraceParams& currDepth, float maxDist, int& rayCasts )
{
	Color result( 0,0,0 );
	const BoundedTriangle& trig = *sourceIsec.mHitTriangle;
	const SurfaceMaterial * smat = trig.trig.material;
	if ( !smat )
		smat = &scene.mDefaultMaterial;

	Vec3 trigTrueNormal = sourceIsec.impactNorm;
	Vec3 trigSmoothNormal = sourceIsec.getPointNormal();
	
	if ( trigTrueNormal.dot( trigSmoothNormal ) < 0 )
  		trigSmoothNormal = - trigSmoothNormal;
 	
	float trueU, trueV;
	trueU =	 sourceIsec.Up0 * trig.trig.u[0]	 +	 sourceIsec.Vp1 * trig.trig.u[1]	 +	 sourceIsec.Wp2 * trig.trig.u[2];
	trueV =	 sourceIsec.Up0 * trig.trig.v[0]	 +	 sourceIsec.Vp1 * trig.trig.v[1]	 +	 sourceIsec.Wp2 * trig.trig.v[2];
	
	// import only through the trueNormal !!!!
	// because correction of vertNormal would be correct !
	VertexMaterial material;
	smat->getVertexMaterial( material, trueU, trueV, trigTrueNormal );
	if ( !smat->mNormalTex || smat->mNormalTex->totalCount < 4 )
	{
		material.vertNormal = trigSmoothNormal;
	}
	else
	{
		if( trigTrueNormal.dot( material.vertNormal ) < 0 )
			material.vertNormal = - material.vertNormal;

		material.vertNormal += trigSmoothNormal - trigTrueNormal;
		material.vertNormal.voidNormalize();
	}
	
	// 3 correction's of normals of trig
	// must use one half-space normals - correct sided normals;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
//DIFFUSE

	if ( material.diffK > 0 )
	{
		int lc = scene.getLightsCount();

		/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
		//DIRECT LAMBERT_LIGHTING

		for( int i=0; i<lc; i++ )
		{
			Color directLight( 0, 0, 0 );
			const Light& light = scene.getLight( i );
			
			//ATTENTION - use volatile, because VS could generate incorrect code!
			volatile bool useSampledLight = bool(light.radius > 0.00001f) && currDepth.maxDirectLightSamples() > 0;

			float counter = 0;
			if ( ! useSampledLight )
			{
 				Vec3 dir = (light.position - sourceIsec.mContact).getNormalized();
				if ( trigTrueNormal.dot( dir ) <= 0 )
					continue;

				float f = material.vertNormal.dot( dir );

				rayCasts++;
				if ( f > 0 && ! scene.rayCastPoint( sourceIsec.mContact, light.position, &trig ) )
				{
					counter += 1;
					directLight += light.getResultPowerOnDist( (sourceIsec.mContact - light.position).magnitude() ) * material.diffC * f;
				}
			}
			if ( useSampledLight )
			{
				const float toLightCenterDist = ( light.position - sourceIsec.mContact ).magnitude();
				if ( toLightCenterDist < light.radius )
				{
					counter += 1;
					directLight += light.color * material.diffC;
				}
				else
				{
					for( int i=0; i<currDepth.maxDirectLightSamples(); ++i )
					{
						Vec3 randomPoint = light.sampleRandomDirectHemiSphere( sourceIsec.mContact );
							
						Vec3 dir = (randomPoint - sourceIsec.mContact).getNormalized();
						if ( trigTrueNormal.dot( dir ) <= 0 )
							continue;

						float f = material.vertNormal.dot( dir );

						rayCasts++;
						if ( f > 0  && ! scene.rayCastPoint( sourceIsec.mContact, randomPoint, &trig ) )
						{
							counter += 1;
							directLight += light.getResultPowerOnDist( (sourceIsec.mContact - randomPoint).magnitude() ) * material.diffC * f;
						}
					}

					if ( counter > 1 )
						directLight /= counter;
				}

			} // if ( useSampledLight )
						
			result += directLight;

		} // direct lighting (sampled or not)
	
		/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
		//DIFFUSE LIGHTING

		if ( inRay.diffBounces > 0 )
		{
			Color diffuseColor( 0,0,0 );
			for( int i=0; i<currDepth.maxDiffSamples(); ++i )
			{			
				Ray newRay = Ray::randomHemiSphereRay( inRay, Ray::diffuse, material.vertNormal, sourceIsec.mContact );

				if ( newRay.dir.dot( trigTrueNormal ) < 0 )
				{
					// make fake-offset from trig
					newRay.origin += trigTrueNormal * TRIANGLE_RAYCAST_OFFSET;
					newRay.dir.rotateToVector3( Angle(ALMOST_HALF_PI), trigTrueNormal, inRay.dir );
				
					diffuseColor += scene.geometryShaderInvokePrimeCast( newRay, trig, currDepth );
				}
				else
					diffuseColor += scene.geometryShaderInvoke( newRay, &trig, currDepth );
				
			}

			diffuseColor /= float ( currDepth.maxDiffSamples() );
			result += diffuseColor * material.diffC;
		}

		result *= material.diffK;
	}

		
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
//REFLECTION

		
	if ( material.reflectK > 0 && inRay.reflBounces > 0 )
	{
		Color reflectColor( 0,0,0 );
		
		Vec3 idealReflectDir = reflect( inRay.dir, material.vertNormal );

		if ( material.glossReflect > 0.99999f )
 		{
 			Ray newRay( inRay, Ray::reflect, sourceIsec.mContact, idealReflectDir );

			if ( newRay.dir.dot( trigTrueNormal ) < 0 )
			{
				// make fake-offset from trig
				newRay.origin += trigTrueNormal * TRIANGLE_RAYCAST_OFFSET;
				Vec3 oldDir = newRay.dir;
				newRay.dir.rotateToVector3( Angle(ALMOST_HALF_PI), trigTrueNormal, inRay.dir );
				
				reflectColor += scene.geometryShaderInvoke( newRay, &trig, currDepth );
			}
			else
				reflectColor += scene.geometryShaderInvoke( newRay, &trig, currDepth );
 		}
 		else
		{
			for( int i=0; i<currDepth.maxReflectSamples(); ++i )
			{				
				float glossRand = randf( material.glossReflect, 1 );
				Ray newRay( Ray::randomConeRay( inRay, Ray::reflect, Ray( sourceIsec.mContact, idealReflectDir, Ray::reflect ), glossRand ) );

				if ( newRay.dir.dot( trigTrueNormal ) < 0 )
				{
					// make fake-offset from trig
					newRay.origin += trigTrueNormal * TRIANGLE_RAYCAST_OFFSET;
					newRay.dir.rotateToVector3( Angle(ALMOST_HALF_PI), trigTrueNormal, inRay.dir );

					reflectColor += scene.geometryShaderInvoke( newRay, &trig, currDepth );
				}
				else
					reflectColor += scene.geometryShaderInvoke( newRay, &trig, currDepth );
			}
			
			reflectColor /= float ( currDepth.maxReflectSamples() );
		}	
	
		reflectColor = reflectColor * material.reflectC; // filtering!
		reflectColor *= material.reflectK;
	
		result += reflectColor;		
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
//REFRACTION


	if ( material.refractK > 0 && inRay.refrBounces > 0 )
	{
		Color refractColor( 0,0,0 );

		Vec3 idealRefractDir = refract( inRay.dir, material.vertNormal, material.ior );

		if ( material.glossRefract > 0.99999f )
		{
			Ray newRay( inRay, Ray::refract, sourceIsec.mContact, idealRefractDir );
			if ( newRay.dir.dot( trigTrueNormal ) > 0 )
			{
				// make fake-offset from trig
				// -= - because from the other side then ray, raycasted to ray-side
				newRay.origin -= trigTrueNormal * TRIANGLE_RAYCAST_OFFSET;
				newRay.dir.rotateToVector3( Angle(ALMOST_HALF_PI), -trigTrueNormal, inRay.dir );

				refractColor += scene.geometryShaderInvoke( newRay, &trig, currDepth );
			}
			else
				refractColor += scene.geometryShaderInvoke( newRay, &trig, currDepth );
		}
		else
		{
			for( int i=0; i<currDepth.maxRefractSamples(); ++i )
			{	
				float glossRand = randf( material.glossRefract, 1 );
				Ray newRay( Ray::randomConeRay( inRay, Ray::refract, Ray( sourceIsec.mContact, idealRefractDir, Ray::refract ), glossRand ) );
				if ( newRay.dir.dot( trigTrueNormal ) > 0 )
				{
					// make fake-offset from trig
					// -= - because from the other side then ray, raycasted to ray-side
					newRay.origin -= trigTrueNormal * TRIANGLE_RAYCAST_OFFSET;
					newRay.dir.rotateToVector3( Angle(ALMOST_HALF_PI), -trigTrueNormal, inRay.dir );

					refractColor += scene.geometryShaderInvoke( newRay, &trig, currDepth );
				}
				else
					refractColor += scene.geometryShaderInvoke( newRay, &trig, currDepth );
			}

			refractColor /= float ( currDepth.maxRefractSamples() );
		}
			
		refractColor = refractColor * material.refractC; // filtering!
		refractColor *= material.refractK;
		
		result += refractColor;		
	}
	

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
//EMITTED
	
	
	result += material.emitC;
		

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
//PHONG angle-attenuation


	if ( inRay.type == Ray::diffuse )
	{
		float f = fabs( material.vertNormal.dot( inRay.dir ) );
		result *= f;
	}
		
	
	return result;

} // PhongSurfaceShader::onGeometry


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Color DefaultEnvironmentShader::onRay( const Ray& outerRay )
{
	return map.intersect( outerRay );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
