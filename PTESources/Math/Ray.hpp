#pragma once

#include "PTESources/Math/Vector3.hpp"
#include "PTESources/Math/Quaternion.hpp"
#include "PTESources/RenderSystem/Color.hpp"
#include "PTESources/Math/Utility.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

struct SurfaceShader;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// used optimized Ray for AABB's
// http://people.csail.mit.edu/amy/papers/box-jgt.pdf

struct Ray
{
	enum Type{ diffuse, refract, reflect, camera };


	__host__ __device__  Ray( const Vec3& orig, const Vec3& d, Type newType )
		:	origin( orig )
		,	dir( d )
		,	type( newType )
	{
		set();
	}
		
	__host__ __device__  Ray( const Ray& prevRay, Type newType, const Vec3& orig, const Vec3& d )
		:	origin( orig )
		,	dir( d )
		,	type( newType )
	{
		set();

		diffBounces = prevRay.diffBounces;
		reflBounces = prevRay.reflBounces;
		refrBounces = prevRay.refrBounces;

		switch( newType )
		{
			case Ray::diffuse : diffBounces -= 1; break;
			case Ray::reflect : reflBounces -= 1; break;
			case Ray::refract : refrBounces -= 1; break;
		}		
	}

// 	static Ray randomHemiSphereRay( const Vec3& normal, const Vec3& orig )
// 	{
// 		Vec3 res( randf_11(), randf_11(), randf_11() );
// 		res.voidNormalize();
// 		
// 		if( res.dot( normal ) < 0 )
// 			res *= -1.f;
// 
// 		return Ray( orig, res );
// 	}


	__host__ __device__ static Ray randomHemiSphereRay( const Ray& prevRay, Type newType, const Vec3& normal, const Vec3& orig )
	{
// 		Vec3 newDir( 10*randf_11(), 10*randf_11(), 10*randf_11() );
// 		newDir.voidNormalize();
// 
// 		if( newDir.dot( normal ) < 0 )
// 			newDir *= -1.f;

		Vec3 rotator;
		LGenRandRay:
		rotator.set( randf_11(), randf_11(), randf_11() );
		rotator.voidNormalize();

		if( fabs( rotator.dot( normal ) ) > 0.999f )
			goto LGenRandRay;

		Vec3 newDir = normal;

		float cosA = 1- ( randf01() );
		newDir.rotate( cosA, rotator );	

		return Ray( prevRay, newType, orig, newDir );
	}


// 	static Ray randomConeRay( const Ray& dir, float cosA )
// 	{
// 		Vec3 rotator;
// 		LGenRandRay:
// 		rotator.set( randf_11(), randf_11(), randf_11() );
// 		rotator.voidNormalize();
// 
// 		if( fabs( rotator.dot( dir.dir ) ) > 0.999f )
// 			goto LGenRandRay;
// 
// 		Vec3 newDir = dir.dir;
// 		newDir.rotate( cosA, rotator );
// 		
// 		newDir.voidNormalize();
// 
// 		return Ray( dir.origin, newDir );
// 	}

	__host__ __device__ static Ray randomConeRay( const Ray& prevRay, Type newType, const Ray& dir, float cosA )
	{
		Vec3 rotator;
		LGenRandRay:
		rotator.set( randf_11(), randf_11(), randf_11() );
		rotator.voidNormalize();

		if( fabs( rotator.dot( dir.dir ) ) > 0.999f )
			goto LGenRandRay;

		Vec3 newDir = dir.dir;
		
		newDir.rotate( cosA, rotator );		
		newDir.voidNormalize();

		return Ray( prevRay, newType, dir.origin, newDir );
	}


private:

	__host__ __device__ void set()
	{
		reflBounces = 0;
		refrBounces = 0;
		diffBounces = 0;
		dir.voidNormalize();

		invDir.x = clamp( -9E+20f, 9E+20f, 1 / dir.x );
		invDir.y = clamp( -9E+20f, 9E+20f, 1 / dir.y );
		invDir.z = clamp( -9E+20f, 9E+20f, 1 / dir.z );

		sign[0] = (invDir.x < 0);
		sign[1] = (invDir.y < 0);
		sign[2] = (invDir.z < 0);
	}

public:

	Type type;
	int diffBounces;
	int reflBounces;
	int refrBounces;

	int sign[3];
	Vec3 invDir;
	Vec3 origin;
	Vec3 dir;

}; // class Ray


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
