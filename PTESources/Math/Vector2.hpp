#pragma once

#include "PTESources/Math/Utility.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{
			
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Vec2
{

public:

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
	

	__host__ __device__ Vec2()
	{}

	__host__ __device__ explicit Vec2(float _s)
		:	x( _s )
		,	y( _s )
	{}

	__host__ __device__ Vec2(float _x, float _y)
		:	x( _x )
		,	y( _y )
	{}

	__host__ __device__ Vec2(const Vec2& _v)
		:	x( _v.x )
		,	y( _v.y )
	{}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ float& operator[]( int _index )
	{
		Assert( _index >= 0 && _index <= 1 );
		return (&x)[ _index ];
	}

	__host__ __device__ float operator[]( int _index ) const
	{
		Assert( _index >= 0 && _index <= 1 );
		return (&x)[ _index ];
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ Vec2& operator = ( const Vec2& _v )
	{
		x = _v.x; y = _v.y;
		return *this;
	}
	
	__host__ __device__ bool operator == ( const Vec2& _v ) const
	{
		return x == _v.x && y == _v.y;
	}

	__host__ __device__ bool operator != ( const Vec2& _v ) const
	{
		return x != _v.x || y != _v.y;
	}

	__host__ __device__ bool isZero() const
	{
		return x == 0.0f && y == 0.0f;
	}
	
	__host__ __device__ bool isFinite() const
	{
		return PTE::isFinite( x ) && PTE::isFinite( y );
	}

	__host__ __device__ bool isNormalized() const
	{
		return isFinite() && fabs( magnitude() - 1.0f ) < 0.00001f;
	}

	__host__ __device__ float magnitudeSquared() const
	{
		return x * x + y * y;
	}

	__host__ __device__ float magnitude() const
	{
		return sqrt( magnitudeSquared() );
	}

	__host__ __device__ Vec2 operator -() const
	{
		return Vec2( -x, -y );
	}

	__host__ __device__ Vec2 operator + ( const Vec2& _v ) const
	{
		return Vec2( x + _v.x, y + _v.y );
	}

	__host__ __device__ Vec2 operator - ( const Vec2& _v ) const
	{
		return Vec2( x - _v.x, y - _v.y );
	}

	__host__ __device__ Vec2 operator * ( float f ) const
	{
		return Vec2( x * f, y * f );
	}

	__host__ __device__ Vec2 operator / ( float f ) const
	{
		f = 1.0f / f;
		return Vec2( x * f, y * f);
	}

	__host__ __device__ Vec2& operator += ( const Vec2& _v )
	{
		x += _v.x;
		y += _v.y;
		return *this;
	}

	__host__ __device__ Vec2& operator -= ( const Vec2& _v )
	{
		x -= _v.x;
		y -= _v.y;
		return *this;
	}

	__host__ __device__ Vec2& operator *= ( float f )
	{
		x *= f;
		y *= f;
		return *this;
	}

	__host__ __device__ Vec2& operator /= ( float f )
	{
		f = 1.0f / f;
		x *= f;
		y *= f;
		return *this;
	}
	

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ float dot( const Vec2& _v ) const		
	{	
		return x * _v.x + y * _v.y;				
	}

	__host__ __device__ Vec2 getNormalized() const
	{
		const float magnitude = magnitudeSquared();
		return magnitude > 0
			?	*this / sqrt( magnitude )
			:	Vec2( 0, 0 )
		;
	}

	__host__ __device__ float normalize()
	{
		const float magnitude = this->magnitude();
		if ( magnitude > 0 ) 
			*this /= magnitude;

		return magnitude;
	}

	__host__ __device__ void voidNormalize()
	{
		const float magnitude = this->magnitude();
		if ( magnitude > 0 ) 
		{
			x /= magnitude;
			y /= magnitude;
		}
	}
	
	__host__ __device__ Vec2 multiply( const Vec2& _v ) const
	{
		return Vec2( x * _v.x, y * _v.y );
	}

	__host__ __device__ Vec2 minimal(const Vec2& _v) const
	{ 
		return Vec2( PTE::minimal( x, _v.x ), PTE::minimal( y, _v.y ) );	
	}

	__host__ __device__ float minElement()	const
	{
		return PTE::minimal(x, y);
	}
	
	__host__ __device__ Vec2 maximal(const Vec2& _v) const
	{ 
		return Vec2( PTE::maximal(x, _v.x ), PTE::maximal( y,_v.y ) );	
	} 

	__host__ __device__ float maxElement()	const
	{
		return PTE::maximal(x, y);
	}

public:

	float x;
	float y;

}; // class Vector2


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ static inline Vec2 operator * ( float f, const Vec2& _v )
{
	return _v * f;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
