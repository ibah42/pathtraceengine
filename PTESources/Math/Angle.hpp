#pragma once


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{
		
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class Angle
{
public:

	__host__ __device__ Angle( float _radians )
		:	mRadians( _radians )
	{}
	
	__host__ __device__ inline static Angle buildRadians( float _radians )
	{
		return Angle( _radians );
	}
	
	__host__ __device__ inline static Angle buildDegrees( float _degrees )
	{
		Angle a;
		a.setDegrees( _degrees );
		return a;
	}

	__host__ __device__ inline Angle()
		:	mRadians(0)
	{}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	__host__ __device__ inline bool operator < ( const Angle _a )
	{
		return mRadians < _a.mRadians;
	}

	__host__ __device__ inline bool operator > ( const Angle _a )
	{
		return mRadians > _a.mRadians;
	}

	__host__ __device__ inline bool operator == ( const Angle _a )
	{
		return mRadians == _a.mRadians;
	}

	__host__ __device__ inline bool operator != ( const Angle _a )
	{
		return mRadians != _a.mRadians;
	}

	__host__ __device__ inline Angle operator + ( const Angle _a )
	{
		return Angle( mRadians + _a.mRadians );
	}

	__host__ __device__ inline Angle operator - ( const Angle _a )
	{
		return Angle( mRadians - _a.mRadians );
	}

	__host__ __device__ inline Angle operator - ()
	{
		return Angle( - mRadians );
	}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ friend inline Angle operator * ( float f, Angle a )
	{
		return Angle( f * a.mRadians );
	}

	__host__ __device__ friend inline Angle operator * ( Angle a, float f )
	{
		return Angle( f * a.mRadians );
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	__host__ __device__ inline float radians() const
	{
		return mRadians;
	}

	__host__ __device__ inline float degrees() const
	{
		return mRadians / Pi * 180.0f;
	}

	__host__ __device__ inline void setRadians( float _radians )
	{
		mRadians = _radians;
	}

	__host__ __device__ inline void setDegrees( float _degrees )
	{
		mRadians = _degrees * Pi / 180.0f;
	}

	__host__ __device__ inline float cos() const
	{
		return ::cos( mRadians );
	}
	
	__host__ __device__ inline float sin() const
	{
		return ::sin( mRadians );
	}
	
	__host__ __device__ inline float tan() const
	{
		return ::tan( mRadians );
	}
	
	__host__ __device__ inline static Angle atan( float _trygValue )
	{
		return Angle ( ::atan( _trygValue ) );
	}

	__host__ __device__ inline static Angle atan2( float _trygValue1, float _trygValue2 )
	{
		return Angle ( ::atan2( _trygValue1, _trygValue2 ) );
	}

	__host__ __device__ inline static Angle acos( float _trygValue )
	{
		if ( _trygValue >= 1 )
			return Angle ( ::acos( 1.f ) );

		if ( _trygValue <= -1 )
			return Angle ( ::acos( -1.f ) );

		return Angle ( ::acos( _trygValue ) );
	}

	__host__ __device__ static Angle asin( float _trygValue )
	{
		if ( _trygValue >= 1 )
			return Angle ( ::asin( 1.f ) );

		if ( _trygValue <= -1 )
			return Angle ( ::asin( -1.f ) );

		return Angle ( ::asin( _trygValue ) );
	}


	__host__ __device__ template< typename _TVec3 >
 	static Angle angleBetweenVectors3( const _TVec3& _1, const _TVec3& _2 )
 	{		
 		float lenProduct = Basement::magnitudeXZ( _1 ) * Basement::magnitudeXZ( _2 );
 		lenProduct = Basement::returnCorrectDivider( lenProduct );
 
 		float f = ( _1.x * _2.x  +  _1.z * _2.z ) / lenProduct;
  		f = clamp( -1.0f, 1.0f, f );
 		return Angle::acos( f );
 	}

private:

	float mRadians;

}; // class Angle


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
