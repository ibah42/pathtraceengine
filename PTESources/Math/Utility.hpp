#pragma once

#include <cassert>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{
	
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	const float Pi		=	3.14159265359f;
	const float TwoPi	=	Pi * 2.0f;

	const float G		=	9.80665f;


	typedef long long int64;
	typedef unsigned long long uint64;
	typedef unsigned char Byte;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

//http://habrahabr.ru/company/ifree/blog/197520/
#define PARALLEL_SPECULATIVE_EXECUTION_BARRIER  _ReadWriteBarrier();

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__host__ __device__ void Assert( T data )
{
#ifdef _DEBUG
	if ( data == 0 )
	{
		//throw std::exception("Ibah42 ASSERT");
		assert(false);
	}
#endif // _DEBUG
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ inline float sqrt( float v )
{
	return ::sqrt( v );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ inline bool	isFinite( float v )
{
	return
			v < 999999.f * 999999.f
		&&	v > - 999999.f * 999999.f
	;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
__host__ __device__ inline T linearInterpolation ( T a, T b, float mix )
{
	return a + ( b - a ) * mix; 
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
__host__ __device__ inline T
cubicInterpolation ( T P0, T P1, T P2, T P3, float mix )
{
	float mix_sq = mix * mix;
	T PH = ( P3 - P2 - P0 + P1 );
	return( 
			PH * mix * mix_sq
		+	( P0 - P1 - PH ) * mix_sq
		+	( P2 - P0 ) * mix
		+	P1
	); 
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T, typename T1 >
__host__ __device__ inline T
clamp ( T1 downTresh, T1 upTresh, T value )
{
	return
		value >= downTresh
		?	(
				value <= upTresh
				?	value
				:	upTresh
			)
		:	downTresh
	;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template <class T>
__host__ __device__ inline void swapValues ( T & a, T & b )
{
	T c = a;
	a = b;
	b = c;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ inline bool cmpEqual ( float a, float b, float eps )
{
	return fabs( a-b ) < eps;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T > 
__host__ __device__ inline T minimal( T a, T b )
{
	volatile int cmp = a>=b;
	return (!cmp)*a + (cmp)*b;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__host__ __device__ inline T maximal( T a, T b )
{
	volatile int cmp = a>b;
	return (!cmp)*b + cmp * a;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TVec3 >
__host__ __device__ inline TVec3 
getTriangleLocalBounds( const TVec3& v1, const TVec3& v2, const TVec3& v3 )
{
	float xmin = minimal( v1.x, v2.x );
	xmin = minimal( xmin, v3.x );

	float ymin = minimal( v1.y, v2.y );
	ymin = minimal( ymin, v3.y );

	float zmin = minimal( v1.z, v2.z );
	zmin = minimal( zmin, v3.z );


	float xmax = maximal( v1.x, v2.x );
	xmax = maximal( xmax, v3.x );

	float ymax = maximal( v1.y, v2.y );
	ymax = maximal( ymax, v3.y );

	float zmax = maximal( v1.z, v2.z );
	zmax = maximal( zmax, v3.z );

	Assert( xmax - xmin >= 0 );
	Assert( ymax - ymin >= 0 );
	Assert( zmax - zmin >= 0 );

	return TVec3( xmax-xmin, ymax-ymin, zmax-zmin );

} // getTriangleLocalBounds


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// reflect to the n.dot(dir) > 0 - makes refraction effect!
// only if n.dot(dir) <= 0 it takes origin sence!
//http://users.skynet.be/bdegreve/writings/reflection_transmission.pdf
template< typename TVec >
__host__ __device__ inline TVec
reflect(const TVec& dir, const TVec& normal)
{
	TVec temp = normal * ( dir.dot(normal) * (-2.f) );
	temp += dir;
	temp.voidNormalize(); 

	return temp;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TVec >
__host__ __device__ inline float
vecsCosAngle(const TVec& v1, const TVec& v2 )
{
	return v1.dot( v2 ) / sqrt( v1.magnitudeSquared() * v2.magnitudeSquared() );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ inline int rand( int max )
{
	return ::rand() % max;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ inline int rand( int min, int max )
{
	return ( ::rand() % (max-min) ) + min;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ inline float randf01()
{
	return (float) ::rand() / (float) RAND_MAX;
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ inline float randf( float min, float max )
{
	return min + randf01() * (max-min) ;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ inline float randf_11()
{
	return 1.f - randf01() * 2.f;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//check-value exists in bounds ( check in [left;right] )
template< typename T >
__host__ __device__ bool inline
inSegment( T left, T right, T check )
{
	Assert( left <= right );
	return check >= left && check <= right;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//http://users.skynet.be/bdegreve/writings/reflection_transmission.pdf
template< typename TVec >
__host__ __device__ inline TVec
refract(const TVec& dir, const TVec& normal, float ior)
{
	float cos_phi_i = - ( dir.dot(normal) );
	float sin_phi_t_2 = ior*ior * ( 1 - cos_phi_i * cos_phi_i );

	TVec temp = ior * dir + normal * ( ior * cos_phi_i - sqrt( 1 - sin_phi_t_2 ) );
	temp.voidNormalize(); 

	return temp;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



