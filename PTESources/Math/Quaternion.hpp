#pragma once

#include "PTESources/Math/Vector3.hpp"
#include "PTESources/Math/Angle.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

		
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	class Matrix33;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Quaternion
{
public:

	__host__ __device__ explicit Quaternion( const Matrix33& _m );

	__host__ __device__ Quaternion()
	{}

	__host__ __device__ Quaternion( float _x, float _y, float _z, float _w )
		:	x( _x )
		,	y( _y )
		,	z( _z )
		,	w( _w )
	{}

	__host__ __device__ Quaternion( float _angleRadians, const Vec3& _unitAxis )
	{
		Assert( fabs ( 1.0f - _unitAxis.magnitude() ) < 0.00001f );
		const Angle a = Angle::buildRadians( _angleRadians * 0.5f );
		const float s = a.sin();
		w = a.cos();
		x = _unitAxis.x * s;
		y = _unitAxis.y * s;
		z = _unitAxis.z * s;
	}

	__host__ __device__ Quaternion( const Quaternion& _v )
		:	x( _v.x )
		,	y( _v.y )
		,	z( _v.z )
		,	w( _v.w )
	{}

	__host__ __device__ bool isFinite() const
	{
		return
				PTE::isFinite( x ) 
			&&	PTE::isFinite( y ) 
			&&	PTE::isFinite( z )
			&&	PTE::isFinite( w )
		;
	}
	
	__host__ __device__ bool isUnit() const
	{
		return isFinite() && fabs( magnitude()-1.0f ) < 0.00001f;
	}
	
	//brief returns true if finite and magnitude ~= 1.0f
	__host__ __device__ bool isSane() const
	{
		return isFinite() && fabs( magnitude() - 1.0f ) < 0.01f;
	}

	// converts this quaternion to angle-axis representation
	__host__ __device__ void toRadiansAndUnitAxis( float& _angle, Vec3& _axis ) const
	{
		const float quatEpsilon = 0.000001f;
		const float s2 = x*x + y*y + z*z;
		if( s2 < quatEpsilon * quatEpsilon )  // can't extract a sensible axis
		{
			_angle = 0;
			_axis = Vec3( 1, 0, 0 );
		}
		else
		{
			const float s = 1.f / sqrt( s2 );
			_axis = Vec3( x*s, y*s, z*s ); 
			_angle = fabs( w ) < quatEpsilon
				?	Pi
				:	Angle::atan2( s2 * s, w ).radians() * 2
			;
		}
	}

	// angle between quaternion and the identity quaternion
	__host__ __device__ Angle getAngle() const
	{
		return Angle::acos( w ) * 2.0f;
	}

	__host__ __device__ Angle getAngle( const Quaternion& q ) const
	{
		return Angle::acos( dot( q ) ) * 2.0f;
	}

	__host__ __device__ float magnitudeSquared() const
	{
		return x*x + y*y + z*z + w*w;
	}

	__host__ __device__ float dot( const Quaternion& q ) const
	{
		return x * q.x + y * q.y + z * q.z  + w * q.w;
	}

	__host__ __device__ Quaternion getNormalized() const
	{
		const float s = 1.0f / magnitude();
		return Quaternion( x*s, y*s, z*s, w*s );
	}

	__host__ __device__ float magnitude() const
	{
		return sqrt( magnitudeSquared() );
	}

	__host__ __device__ float normalize()
	{
		float magnitude = this->magnitude();
		if ( cmpEqual( magnitude, 0.0f, 0.001f ) )
		{
			float inverseMagnitude = 1.0f / magnitude;

			x *= inverseMagnitude;
			y *= inverseMagnitude;
			z *= inverseMagnitude;
			w *= inverseMagnitude;
		}
		return magnitude;
	}

	__host__ __device__ Quaternion getConjugate() const
	{
		return Quaternion( -x, -y, -z, w );
	}

	__host__ __device__ Vec3 getBasisVectorX()	const
	{	
		const float x2 = x * 2;
		const float w2 = w * 2;

		return Vec3(
				( w * w2 ) + x * x2 - 1.0f 
			,	( z * w2 ) + y * x2
			,	(-y * w2 ) + z * x2
		);
	}

	__host__ __device__ Vec3 getBasisVectorY()	const 
	{	
		const float y2 = y * 2;
		const float w2 = w * 2;
		return Vec3(
				(-z * w2 ) + x * y2
			,	( w * w2 ) + y * y2 - 1.0f
			,	( x * w2 ) + z * y2
		);
	}

	__host__ __device__ Vec3 getBasisVectorZ() const	
	{	
		const float z2 = z * 2;
		const float w2 = w * 2;
		return Vec3(
				( y * w2) + x * z2
			,	(-x * w2) + y * z2
			,	( w * w2) + z * z2 - 1.0f
		);
	}

	__host__ __device__ const Vec3 rotate( const Vec3& _v ) const
	{
		const float vx = 2 * _v.x;
		const float vy = 2 * _v.y;
		const float vz = 2 * _v.z;
		const float w2 = w*w - 0.5f;
		const float dot2 = ( x*vx + y*vy + z*vz );
		return Vec3(
				vx*w2 + (y * vz - z * vy)*w + x*dot2 
			,	vy*w2 + (z * vx - x * vz)*w + y*dot2 
			,	vz*w2 + (x * vy - y * vx)*w + z*dot2
		);
		/*
		const Vec3 qv(x,y,z);
		return (v*(w*w-0.5f) + (qv.cross(_v))*w + qv*(qv.dot(_v)))*2;
		*/
	}

	__host__ __device__ const Vec3 rotateInv(const Vec3& _v) const
	{
		const float vx = 2 * _v.x;
		const float vy = 2 * _v.y;
		const float vz = 2 * _v.z;
		const float w2 = w*w - 0.5f;
		const float dot2 = ( x*vx + y*vy + z*vz );
		return Vec3(
				vx*w2 - (y * vz - z * vy)*w + x*dot2
			,	vy*w2 - (z * vx - x * vz)*w + y*dot2 
			,	vz*w2 - (x * vy - y * vx)*w + z*dot2
		);
		/*
		const Vec3 qv(x,y,z);
		return (v*(w*w-0.5f) - (qv.cross(_v))*w + qv*(qv.dot(_v)))*2;
		*/
	}

	__host__ __device__ Quaternion&	operator = (const Quaternion& p)
	{
		x = p.x;
		y = p.y;
		z = p.z;
		w = p.w;
		return *this;
	}

	__host__ __device__ Quaternion& operator *= (const Quaternion& q)
	{
		const float tx = w*q.x + q.w*x + y*q.z - q.y*z;
		const float ty = w*q.y + q.w*y + z*q.x - q.z*x;
		const float tz = w*q.z + q.w*z + x*q.y - q.x*y;

		w = w*q.w - q.x*x - y*q.y - q.z*z;
		x = tx;
		y = ty;
		z = tz;

		return *this;
	}

	__host__ __device__ Quaternion& operator += (const Quaternion& q)
	{
		x += q.x;
		y += q.y;
		z += q.z;
		w += q.w;

		return *this;
	}

	__host__ __device__ Quaternion& operator -= ( const Quaternion& q )
	{
		x -= q.x;
		y -= q.y;
		z -= q.z;
		w -= q.w;

		return *this;
	}

	__host__ __device__ Quaternion& operator *= (const float f)
	{
		x *= f;
		y *= f;
		z *= f;
		w *= f;

		return *this;
	}

	__host__ __device__ Quaternion operator * ( const Quaternion& q ) const
	{
		return Quaternion(
				w*q.x + q.w*x + y*q.z - q.y*z
			,	w*q.y + q.w*y + z*q.x - q.z*x
			,	w*q.z + q.w*z + x*q.y - q.x*y
			,	w*q.w - q.x*x - y*q.y - q.z*z
		);
	}

	__host__ __device__ Quaternion operator + ( const Quaternion& q ) const
	{
		return Quaternion( x+q.x, y+q.y, z+q.z, w+q.w );
	}

	__host__ __device__ Quaternion operator - () const
	{
		return Quaternion( -x, -y, -z, -w );
	}


	__host__ __device__ Quaternion operator - (const Quaternion& q) const
	{
		return Quaternion( x-q.x, y-q.y, z-q.z, w-q.w );
	}


	__host__ __device__ Quaternion operator * ( float f ) const
	{
		return Quaternion( x*f, y*f, z*f, w*f );
	}

	static Quaternion createIdentity()
	{
		return Quaternion( 0,0,0,1 );
	}

public:

	float x,y,z,w;

}; // class Quaternion

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
