#pragma once

#include "PTESources/Math/Utility.hpp"
#include "PTESources/Math/Ray.hpp"
#include "PTESources/Math/Vector3.hpp"
#include "PTESources/Math/Quaternion.hpp"
#include "PTESources/RenderSystem/Material.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

struct SurfaceShader;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct Triangle
{
	__host__ __device__ Triangle( const Vec3& p0, const Vec3& p1, const Vec3& p2 )
		:	p0( p0 )
		,	p1( p1 )
		,	p2( p2 )
		,	biSided(false)
	{
		material = 0;
		calcNormal();
		Assert( n0.isNormalized() && n1.isNormalized() && n2.isNormalized() );
	}

	__host__ __device__ Triangle( const Vec3& p0, const Vec3& p1, const Vec3& p2, const Vec3& n0, const Vec3& n1, const Vec3& n2 )
		:	p0( p0 )
		,	p1( p1 )
		,	p2( p2 )
		,	n0( n0 )
		,	n1( n1 )
		,	n2( n2 )
		,	biSided(false)
	{
		material = 0;
		trueNormal = ( p0 - p1 ).cross( p0 - p2 );
		square = trueNormal.magnitude();
		trueNormal /= square;

		if ( trueNormal.dot(n0) < 0 || trueNormal.dot(n1) < 0 || trueNormal.dot(n2) < 0 )
			trueNormal = -trueNormal;

		Assert( n0.isNormalized() && n1.isNormalized() && n2.isNormalized() );
	}

	__host__ __device__ inline float getSquare() const	{ return square; }

	__host__ __device__ void calcNormal()
	{
		trueNormal = ( p0 - p1 ).cross( p0 - p2 );
		square = trueNormal.magnitude();
		Assert( square > 0 );
		trueNormal /= square;
	
		n0 = trueNormal;
		n1 = trueNormal;
		n2 = trueNormal;
	}
	
// 	void setDefaultUV()
// 	{
// 		u[0]=0;
// 		v[0]=0;
// 
// 		u[1]=0;
// 		v[1]=1;
// 
// 		u[2]=1;
// 		v[2]=1;
// 	}

	__host__ __device__ void setUV(
		float u0, float u1, float u2,
		float v0, float v1, float v2
	)
	{
		u[0] = u0;
		u[1] = u1;
		u[2] = u2;

		v[0] = v0;
		v[1] = v1;
		v[2] = v2;
	}

public:

	const SurfaceMaterial *material;
	
	//texture coords
	float u[3], v[3];

	//vertexes
	Vec3 p0;
	Vec3 p1;
	Vec3 p2;

	// used as presented normals at points!
	Vec3 n0;
	Vec3 n1;
	Vec3 n2;

	float square;

	// truenormal is alwayes faced to ray ( angle > 0 )
	bool biSided;

	// true normal (bi-sided) of the triangle - used  to know how ray isec's with trig
	Vec3 trueNormal;

}; // class Triangle


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct AABB
{
	enum Enum
	{
			Min = 0
		,	Max = 1
	};

	float pointQuadDist( const Vec3& v ) const
	{
		return (v-center).magnitudeSquared();
	}

	AABB ( const Vec3& min, const Vec3& max )
	{	
		bounds[0] = min;
		bounds[1] = max;
		makeDataOnMinMax();
	}

	AABB ( float xi, float yi, float zi, float xa, float ya, float za )
	{
		bounds[0].set(xi,yi,zi);
		bounds[1].set(xa,ya,za);
		makeDataOnMinMax();
	}
	
	AABB ( const Triangle& t ) { set(t); }

	AABB (){}

	void set( const Vec3& min, const Vec3& max )
	{
		bounds[0] = min;
		bounds[1] = max;
		makeDataOnMinMax();
	}

	void set( float xmin, float ymin, float zmin, float xmax, float ymax, float zmax )
	{
		bounds[0].x = xmin;
		bounds[0].y = ymin;
		bounds[0].z = zmin;

		bounds[1].x = xmax;
		bounds[1].y = ymax;
		bounds[1].z = zmax;

		makeDataOnMinMax();
	}

	bool hasPoint( const Vec3& v )
	{
		if ( v.x < min().x || v.x > max().x )
			return false;
		if ( v.y < min().y || v.y > max().y )
			return false;
		if ( v.z < min().z || v.z > max().z )
			return false;

		return true;
	}

	AABB static buildMinimalBounded( const Vec3& min1, const Vec3& max1, const Vec3& min2, const Vec3& max2 )
	{
		AABB result;
		result.bounds[0].x = maximal( min1.x, min2.x );
		result.bounds[0].y = maximal( min1.y, min2.y );
		result.bounds[0].z = maximal( min1.z, min2.z );

		result.bounds[1].x = minimal( max1.x, max2.x );
		result.bounds[1].y = minimal( max1.y, max2.y );
		result.bounds[1].z = minimal( max1.z, max2.z );

		result.makeDataOnMinMax();

		return result;
	}

	void set( const Triangle& t )
	{
		bounds[0].x = minimal( t.p0.x, t.p1.x );
		bounds[0].x = minimal( bounds[0].x, t.p2.x );

		bounds[0].y = minimal( t.p0.y, t.p1.y );
		bounds[0].y = minimal( bounds[0].y, t.p2.y );

		bounds[0].z = minimal( t.p0.z, t.p1.z );
		bounds[0].z = minimal( bounds[0].z, t.p2.z );


		bounds[1].x = maximal( t.p0.x, t.p1.x );
		bounds[1].x = maximal( bounds[1].x, t.p2.x );

		bounds[1].y = maximal( t.p0.y, t.p1.y );
		bounds[1].y = maximal( bounds[1].y, t.p2.y );

		bounds[1].z = maximal( t.p0.z, t.p1.z );
		bounds[1].z = maximal( bounds[1].z, t.p2.z );

		Assert( bounds[1].x - bounds[0].x >= 0 );
		Assert( bounds[1].y - bounds[0].y >= 0 );
		Assert( bounds[1].z - bounds[0].z >= 0 );

		makeDataOnMinMax();

	} // getTriangleLocalBounds

	const Vec3& min() const	{ return bounds[0]; }
	const Vec3& max() const	{ return bounds[1]; }

private:

	void makeDataOnMinMax()
	{
		Assert( min().x <= max().x	&&	min().y <= max().y	&&	min().z <= max().z );

		center    = min();
		center   += max();
		center   *= 0.5f;

		halfSize  = max();
		halfSize -= min();
		halfSize *= 0.5f;
	}

public:
	
	// 0 == min
	// 1 == max
	//used for AABB-ray isec
	Vec3 bounds[2];

	// used for AABB-AABB isec
	Vec3 center;
	Vec3 halfSize;

}; // struct AABB


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct BoundedTriangle
{
	BoundedTriangle( Vec3& p0, Vec3& p1, Vec3& p2 )
		:	trig( p0 ,p1 ,p2 )
	{
		box.set(trig);
	}

	BoundedTriangle( const Triangle& t )
		:	trig( t )
		,	box( t )
	{}
	
public:

	Triangle trig;
	AABB box;

}; // struct BoundedTriangle



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
