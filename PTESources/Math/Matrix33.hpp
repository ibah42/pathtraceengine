#pragma once

#include "PTESources/Math/Vector3.hpp"
#include "PTESources/Math/Quaternion.hpp"

/*
Short:
- Matrix have base vectors in columns (vectors are column matrices, 3x1 matrices).
- Matrix is physically stored in column major format
- Matrices are concaternated from left


three base vectors a, b and c the matrix is stored as   
|a.x  b.x  c.x|
|a.y  b.y  c.y|
|a.z  b.z  c.z|

Vectors are treated as columns, so the vector v is 
|x|
|y|
|z|

And matrices are applied _before_ the vector (pre-multiplication)
v' = M*v
|x'|   |a.x b.x c.x|   |x|   |a.x * x + b.x * y + c.x * z|
|y'| = |a.y b.y c.y| * |y| = |a.y * x + b.y * y + c.y * z|
|z'|   |a.z b.z c.z|   |z|   |a.z * x + b.z * y + c.z * z|


compatible with 3d rendering APIs (read D3d and OpenGL)
indexing is
|0 3 6|
|1 4 7|
|2 5 8|

index = column * 3 + row -> translates to "M[column][row]"

The mathematical indexing is M_row,column and this is what is used for _-notation 
so _12 is 1st row, second column and operator(row, column)!

*/

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

		
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Matrix33
{
public:

	__host__ __device__ friend Matrix33 operator * ( float, const Matrix33& );

	__host__ __device__ Matrix33()
		:	mColumn0( 0 )
		,	mColumn1( 0 )
		,	mColumn2( 0 )
	{}
	
	__host__ __device__ Matrix33(
			const Vec3& _0
		,	const Vec3& _1
		,	const Vec3& _2
	)
		:	mColumn0( _0 )
		,	mColumn1( _1 )
		,	mColumn2( _2 )
	{}

	__host__ __device__ explicit Matrix33( float values[9] )
		:	mColumn0( values[0], values[1], values[2] )
		,	mColumn1( values[3], values[4], values[5] )
		,	mColumn2( values[6], values[7], values[8] )
	{}

	__host__ __device__ Matrix33( const Matrix33& that )
		:	mColumn0( that.mColumn0 )
		,	mColumn1( that.mColumn1 )
		,	mColumn2( that.mColumn2 )
	{}
			
	__host__ __device__ Matrix33( const Quaternion& q )
	{
		const float x = q.x;
		const float y = q.y;
		const float z = q.z;
		const float w = q.w;

		const float x2 = x + x;
		const float y2 = y + y;
		const float z2 = z + z;

		const float xx = x2 * x;
		const float yy = y2 * y;
		const float zz = z2 * z;

		const float xy = x2 * y;
		const float xz = x2 * z;
		const float xw = x2 * w;

		const float yz = y2 * z;
		const float yw = y2 * w;
		const float zw = z2 * w;

		mColumn0 = Vec3(1.0f - yy - zz, xy + zw, xz - yw);
		mColumn1 = Vec3(xy - zw, 1.0f - xx - zz, yz + xw);
		mColumn2 = Vec3(xz + yw, yz - xw, 1.0f - xx - yy);
	}	
	
	__host__ __device__ Matrix33& operator = ( const Matrix33& that )
	{
		mColumn0 = that.mColumn0;
		mColumn1 = that.mColumn1;
		mColumn2 = that.mColumn2;
		return *this;
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ static Matrix33 createIdentity()
	{
		return Matrix33( Vec3(1,0,0), Vec3(0,1,0), Vec3(0,0,1) );
	}

	__host__ __device__ static Matrix33 createZero()
	{
		return Matrix33( Vec3(0.0f), Vec3(0.0f), Vec3(0.0f) );
	}

	__host__ __device__ static Matrix33 createDiagonal( const Vec3& _v )
	{
		return Matrix33(
				Vec3( _v.x,	0.0f,	0.0f )
			,	Vec3( 0.0f,	_v.y,	0.0f )
			,	Vec3( 0.0f,	0.0f,	_v.z )
		);
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ Matrix33 getTranspose() const
	{
		const Vec3 v0( mColumn0.x, mColumn1.x, mColumn2.x );
		const Vec3 v1( mColumn0.y, mColumn1.y, mColumn2.y );
		const Vec3 v2( mColumn0.z, mColumn1.z, mColumn2.z );

		return Matrix33( v0,v1,v2 );   
	}

	__host__ __device__ Matrix33 getInverse() const
	{
		const float determinant = getDeterminant();
		Matrix33 inverse;

		if( determinant != 0)
		{
			const float inverseDeterminant = 1.0f / determinant;

			inverse.mColumn0.x = inverseDeterminant *	( mColumn1.y * mColumn2.z - mColumn2.y * mColumn1.z );
			inverse.mColumn0.y = inverseDeterminant *-	( mColumn0.y * mColumn2.z - mColumn2.y * mColumn0.z );
			inverse.mColumn0.z = inverseDeterminant *	( mColumn0.y * mColumn1.z - mColumn0.z * mColumn1.y );

			inverse.mColumn1.x = inverseDeterminant *-	( mColumn1.x * mColumn2.z - mColumn1.z * mColumn2.x );
			inverse.mColumn1.y = inverseDeterminant *	( mColumn0.x * mColumn2.z - mColumn0.z * mColumn2.x );
			inverse.mColumn1.z = inverseDeterminant *-	( mColumn0.x * mColumn1.z - mColumn0.z * mColumn1.x );

			inverse.mColumn2.x = inverseDeterminant *	( mColumn1.x * mColumn2.y - mColumn1.y * mColumn2.x );
			inverse.mColumn2.y = inverseDeterminant *-	( mColumn0.x * mColumn2.y - mColumn0.y * mColumn2.x );
			inverse.mColumn2.z = inverseDeterminant *	( mColumn0.x * mColumn1.y - mColumn1.x * mColumn0.y );

			return inverse;
		}
		else
		{
			return createIdentity();
		}
	}

	__host__ __device__ float getDeterminant() const
	{
		return mColumn0.dot( mColumn1.cross( mColumn2 ) );
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ Matrix33 operator - () const
	{
		return Matrix33( -mColumn0, -mColumn1, -mColumn2 );
	}
		
	__host__ __device__ Matrix33 operator + ( const Matrix33& that ) const
	{
		return Matrix33(
				mColumn0 + that.mColumn0
			,	mColumn1 + that.mColumn1
			,	mColumn2 + that.mColumn2
		);
	}

	__host__ __device__ Matrix33 operator - ( const Matrix33& that ) const
	{
		return Matrix33(
				mColumn0 - that.mColumn0
			,	mColumn1 - that.mColumn1
			,	mColumn2 - that.mColumn2
		);
	}

	__host__ __device__ Matrix33 operator*( float f ) const
	{
		return Matrix33( mColumn0 * f, mColumn1 * f, mColumn2 * f );
	}
	
	__host__ __device__ Vec3 operator * ( const Vec3& v ) const
	{
		return transform( v );
	}
	
	__host__ __device__ Matrix33 operator * ( const Matrix33& that ) const
	{
		return Matrix33(
				transform( that.mColumn0 )
			,	transform( that.mColumn1 )
			,	transform( that.mColumn2 )
		);
	}

	__host__ __device__ Matrix33& operator += ( const Matrix33& that )
	{
		mColumn0 += that.mColumn0;
		mColumn1 += that.mColumn1;
		mColumn2 += that.mColumn2;
		return *this;
	}

	__host__ __device__ Matrix33& operator -= ( const Matrix33& that )
	{
		mColumn0 -= that.mColumn0;
		mColumn1 -= that.mColumn1;
		mColumn2 -= that.mColumn2;
		return *this;
	}

	__host__ __device__ Matrix33& operator *= ( float f )
	{
		mColumn0 *= f;
		mColumn1 *= f;
		mColumn2 *= f;
		return *this;
	}
	

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ float operator()( int _row, int _col ) const
	{
		Assert( _row >= 0 && _row < 2 );
		Assert( _col >= 0 && _col < 2 );
		return ( *this )[ _col ][ _row ];
	}

	__host__ __device__ float& operator()( int _row, int _col )
	{
		Assert( _row >= 0 && _row < 2 );
		Assert( _col >= 0 && _col < 2 );
		return ( *this )[ _col ][ _row ];
	}

	// Transform vector by matrix, equal to v' = M*v
	__host__ __device__ Vec3 transform( const Vec3& that ) const
	{
		return mColumn0 * that.x + mColumn1 * that.y + mColumn2 * that.z;
	}

	// Transform vector by matrix transpose, v' = M^t * v
	__host__ __device__ Vec3 transformTranspose( const Vec3& that ) const
	{
		return Vec3(
				mColumn0.dot( that )
			,	mColumn1.dot( that )
			,	mColumn2.dot( that )
		);
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ const float* front() const
	{
		return & mColumn0.x;
	}

	__host__ __device__ Vec3& operator[]( int _num )
	{
		Assert( _num >=0 && _num < 2 );
		return ( &mColumn0 )[ _num ];
	}
	
	__host__ __device__ const Vec3& operator[]( int _num ) const
	{
		Assert( _num >=0 && _num < 2 );
		return ( &mColumn0 )[ _num ];
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

public:

	Vec3 mColumn0;
	Vec3 mColumn1;
	Vec3 mColumn2;

}; // class Matrix33


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ Quaternion::Quaternion( const Matrix33& _m )
{
	float tr = _m(0,0) + _m(1,1) + _m(2,2);
	float h;

	if( tr >= 0 )
	{
		h = sqrt( tr + 1.f );
		w = 0.5f * h;
		h = 0.5f / h;

		x = (_m(2,1) - _m(1,2)) * h;
		y = (_m(0,2) - _m(2,0)) * h;
		z = (_m(1,0) - _m(0,1)) * h;
	}
	else
	{
		int i = 0; 
		
		if (_m(1,1) > _m(0,0))
			i = 1;

		if (_m(2,2) > _m(i,i))
			i = 2;

		switch ( i )
		{
			case 0:
				h = sqrt( ( _m(0,0) - (_m(1,1) + _m(2,2))) + 1 );
				x = float(0.5) * h;
				h = float(0.5) / h;

				y = (_m(0,1) + _m(1,0)) * h; 
				z = (_m(2,0) + _m(0,2)) * h;
				w = (_m(2,1) - _m(1,2)) * h;
				break;

			case 1:
				h = sqrt( ( _m(1,1) - (_m(2,2) + _m(0,0))) + 1);
				y = float(0.5) * h;
				h = float(0.5) / h;

				z = (_m(1,2) + _m(2,1)) * h;
				x = (_m(0,1) + _m(1,0)) * h;
				w = (_m(0,2) - _m(2,0)) * h;
				break;

			case 2:
				h = sqrt((_m(2,2) - (_m(0,0) + _m(1,1))) + 1);
				z = float(0.5) * h;
				h = float(0.5) / h;

				x = (_m(2,0) + _m(0,2)) * h;
				y = (_m(1,2) + _m(2,1)) * h;
				w = (_m(1,0) - _m(0,1)) * h;
				break;

			default:
				x = 0;
				y = 0;
				z = 0;
				w = 0;
				break;

		} // switch ( i )
	}

} // Quaternion::Quaternion


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
