#pragma once

#include "PTESources/Math/Geometry.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Intersection
{
public:

	__host__ __device__ Intersection()
	{
		memset( this, 0, sizeof(Intersection) );
		mType = ContactType::undef;
		normalExtraction = ( &Intersection::calculatedPNandReturn );
	}

	__host__ __device__ void set( const Intersection& that );

	struct ContactType
	{
		enum Enum
		{
				disjoint	=  0	//  disjoint (no intersect)
			,	degenerate	=  1	//	triangle is degenerate ( a segment or point )
			,	onePoint	=  2	//  intersect in unique point 
			,	samePlane	=  3	//  both are in the same plane
			,	undef		=  4
			,	culled		=  5
		};
	};

	__host__ __device__ void setBestIsec( float maxQuadDist, const Intersection& that );
	
	__host__ __device__ static void intersect( Intersection& out, const Ray& ray, const BoundedTriangle& t );
	
	__host__ __device__ static bool intersect( const Ray& ray, const AABB& box, float& isecDist );
	__host__ __device__ static bool intersect( const AABB& b1, const AABB& b2 );

	
	__host__ __device__ bool hasOnePointContact() const { return mType == ContactType::onePoint; }
	__host__ __device__ void resetType() { mType = ContactType::undef; }

	__host__ __device__ const Vec3& getPointNormal() const { return (this->*normalExtraction)(); }

private:

	__host__ __device__ static void intersect( Intersection& out, const Ray& ray, const Triangle& t );

public:

	float Up0, Vp1, Wp2;
	ContactType::Enum mType;
	Vec3 impactNorm;
	Vec3 mContact;
	const BoundedTriangle* mHitTriangle;
	float mQuadDistance;

private:

	typedef const Vec3& (Intersection::*normalExtractionFunc)() const;

	__host__ __device__ const Vec3& getCalculatedPN() const{ return pointNormal; }
	__host__ __device__ const Vec3& calculatedPNandReturn() const;

private:
	
	mutable normalExtractionFunc normalExtraction;
	mutable Vec3 pointNormal;

}; // class Intersection


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
