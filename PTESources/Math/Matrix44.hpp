#pragma once


#include "PTESources/Math/Matrix33.hpp"
#include "PTESources/Math/Transform.hpp"
#include "PTESources/Math/Vector3.hpp"
#include "PTESources/Math/Vector4.hpp"
#include "PTESources/Math/Quaternion.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

		
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Matrix44
{
public:

	__host__ __device__ friend Matrix44 operator*(float f, const Matrix44& m )
	{
		return m * f;
	}

	__host__ __device__ Matrix44()
	{}

	__host__ __device__ Matrix44(
			const Vec4& col0
		,	const Vec4& col1
		,	const Vec4& col2
		,	const Vec4 &col3
	)
		:	mColumn0(col0)
		,	mColumn1(col1)
		,	mColumn2(col2)
		,	mColumn3(col3)
	{}

	__host__ __device__ Matrix44(
			const Vec3& column0
		,	const Vec3& column1
		,	const Vec3& column2
		,	const Vec3& column3
	)
		:	mColumn0( column0, 0 )
		,	mColumn1( column1, 0 )
		,	mColumn2( column2, 0 )
		,	mColumn3( column3, 1 )
	{}

	__host__ __device__ explicit Matrix44( float values[16] ):
			mColumn0( values[0], values[1], values[2], values[3] )
		,	mColumn1( values[4], values[5], values[6], values[7] )
		,	mColumn2( values[8], values[9], values[10], values[11] )
		,	mColumn3( values[12], values[13], values[14], values[15] )
	{
	}

	__host__ __device__ explicit Matrix44( const Quaternion& q)
	{
		const float x = q.x;
		const float y = q.y;
		const float z = q.z;
		const float w = q.w;

		const float x2 = x + x;
		const float y2 = y + y;
		const float z2 = z + z;

		const float xx = x2*x;
		const float yy = y2*y;
		const float zz = z2*z;

		const float xy = x2*y;
		const float xz = x2*z;
		const float xw = x2*w;

		const float yz = y2*z;
		const float yw = y2*w;
		const float zw = z2*w;

		mColumn0 = Vec4(1.0f - yy - zz, xy + zw, xz - yw, 0.0f);
		mColumn1 = Vec4(xy - zw, 1.0f - xx - zz, yz + xw, 0.0f);
		mColumn2 = Vec4(xz + yw, yz - xw, 1.0f - xx - yy, 0.0f);
		mColumn3 = Vec4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	__host__ __device__ explicit Matrix44( const Vec4& _diagonal ):
			mColumn0( _diagonal.x, 0.0f, 0.0f, 0.0f )
		,	mColumn1( 0.0f, _diagonal.y, 0.0f, 0.0f )
		,	mColumn2( 0.0f, 0.0f, _diagonal.z, 0.0f )
		,	mColumn3( 0.0f, 0.0f, 0.0f, _diagonal.w )
	{
	}

	__host__ __device__ Matrix44( const Matrix33& _orientation, const Vec3& _position)
		:	mColumn0( _orientation.mColumn0, 0.0f )
		,	mColumn1( _orientation.mColumn1, 0.0f )
		,	mColumn2( _orientation.mColumn2, 0.0f )
		,	mColumn3( _position, 1.0f )
	{
	}
		
	__host__ __device__ Matrix44( const Transform& t)
	{
		*this = Matrix44( Matrix33( t.q ), t.v );
	}

	__host__ __device__ Matrix44( const Matrix44& that )
		:	mColumn0( that.mColumn0 )
		,	mColumn1( that.mColumn1 )
		,	mColumn2( that.mColumn2 )
		,	mColumn3( that.mColumn3 )
	{}

	__host__ __device__ const Matrix44& operator = ( const Matrix44& that )
	{
		mColumn0 = that.mColumn0;
		mColumn1 = that.mColumn1;
		mColumn2 = that.mColumn2;
		mColumn3 = that.mColumn3;
		return *this;
	}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	__host__ __device__ const float* front() const
	{
		return &mColumn0.x;
	}

	__host__ __device__ Vec4& operator[]( int _num )
	{
		Assert( _num >=0 && _num < 3 );
		return ( &mColumn0 )[ _num ];
	}

	__host__ __device__ const Vec4& operator[]( int _num ) const
	{
		Assert( _num >=0 && _num < 3 );
		return ( &mColumn0 )[ _num ];
	}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ static Matrix44 createIdentity()
	{
		return Matrix44(
				Vec4( 1.0f,0.0f,0.0f,0.0f )
			,	Vec4( 0.0f,1.0f,0.0f,0.0f )
			,	Vec4( 0.0f,0.0f,1.0f,0.0f )
			,	Vec4( 0.0f,0.0f,0.0f,1.0f )
		);
	}

	__host__ __device__ static Matrix44 createZero()
	{
		return Matrix44( Vec4(0.0f), Vec4(0.0f), Vec4(0.0f), Vec4(0.0f) );
	}
	
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ Matrix44 operator - () const
	{
		return Matrix44( -mColumn0, -mColumn1, -mColumn2, -mColumn3 );
	}

	__host__ __device__ Matrix44 operator + ( const Matrix44& that ) const
	{
		return Matrix44(
				mColumn0 + that.mColumn0
			,	mColumn1 + that.mColumn1
			,	mColumn2 + that.mColumn2
			,	mColumn3 + that.mColumn3
		);
	}

	__host__ __device__ Matrix44 operator - ( const Matrix44& that ) const
	{
		return Matrix44(
				mColumn0 - that.mColumn0
			,	mColumn1 - that.mColumn1
			,	mColumn2 - that.mColumn2
			,	mColumn3 - that.mColumn3
		);
	}

	
	__host__ __device__ Matrix44 operator*(float f) const
	{
		return Matrix44(mColumn0*f, mColumn1*f, mColumn2*f, mColumn3*f);
	}


	__host__ __device__ Matrix44 operator * ( const Matrix44& that ) const
	{
		return Matrix44(
				transform(that.mColumn0)
			,	transform(that.mColumn1)
			,	transform(that.mColumn2)
			,	transform(that.mColumn3)
		);
	}

	__host__ __device__ Matrix44& operator += ( const Matrix44& that )
	{
		mColumn0 += that.mColumn0;
		mColumn1 += that.mColumn1;
		mColumn2 += that.mColumn2;
		mColumn3 += that.mColumn3;
		return *this;
	}

	__host__ __device__ Matrix44& operator -= ( const Matrix44& that )
	{
		mColumn0 -= that.mColumn0;
		mColumn1 -= that.mColumn1;
		mColumn2 -= that.mColumn2;
		mColumn3 -= that.mColumn3;
		return *this;
	}

	__host__ __device__ Matrix44& operator *= ( float f )
	{
		mColumn0 *= f;
		mColumn1 *= f;
		mColumn2 *= f;
		mColumn3 *= f;
		return *this;
	}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	__host__ __device__ float operator()( int _row, int _col ) const
	{
		Assert( _row >=0 && _row < 3 );
		Assert( _col >=0 && _col < 3 );
		return (*this)[_col][_row];
	}

	__host__ __device__ float& operator()( int _row, int _col )
	{
		Assert( _row >=0 && _row < 3 );
		Assert( _col >=0 && _col < 3 );
		return (*this)[_col][_row];
	}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
	
	__host__ __device__ Vec4 transform( const Vec4& that ) const
	{
		return mColumn0*that.x + mColumn1*that.y + mColumn2*that.z + mColumn3*that.w;
	}

	__host__ __device__ Vec3 transform( const Vec3& that ) const
	{
		return transform( Vec4( that, 1.0f ) ).getXYZ();
	}

	// Rotate vector by matrix, equal to v' = M*v
	__host__ __device__ Vec4 rotate( const Vec4& that ) const
	{
		return mColumn0*that.x + mColumn1*that.y + mColumn2*that.z;// + column3*0;
	}

	// Rotate vector by matrix, equal to v' = M*v
	__host__ __device__ Vec3 rotate( const Vec3& that ) const
	{
		return rotate( Vec4( that, 1.0f ) ).getXYZ();
	}

	__host__ __device__ Matrix44 getTranspose() const
	{
		return Matrix44(
				Vec4(mColumn0.x, mColumn1.x, mColumn2.x, mColumn3.x)
			,	Vec4(mColumn0.y, mColumn1.y, mColumn2.y, mColumn3.y)
			,	Vec4(mColumn0.z, mColumn1.z, mColumn2.z, mColumn3.z)
			,	Vec4(mColumn0.w, mColumn1.w, mColumn2.w, mColumn3.w)
		);
	}

	__host__ __device__ Vec3 getBasis( int _num ) const
	{
		Assert( _num >=0 && _num < 3 );
		return (&mColumn0)[ _num ].getXYZ();
	}

	__host__ __device__ Vec3 getPosition() const
	{
		return mColumn3.getXYZ();
	}

	__host__ __device__ void setPosition( const Vec3& _position )
	{
		mColumn3.x = _position.x;
		mColumn3.y = _position.y;
		mColumn3.z = _position.z;
	}
	
	__host__ __device__ void scale( const Vec4& _v )
	{
		mColumn0 *= _v.x;
		mColumn1 *= _v.y;
		mColumn2 *= _v.z;
		mColumn3 *= _v.w;
	}

	__host__ __device__ Matrix44 inverseRT(void) const
	{
		Vec3 r0(	mColumn0.x, mColumn1.x, mColumn2.x );
		Vec3	r1( mColumn0.y, mColumn1.y, mColumn2.y );
		Vec3 r2( mColumn0.z, mColumn1.z, mColumn2.z );

		return Matrix44(
				r0
			,	r1
			,	r2
			,	-( r0 * mColumn3.x + r1 * mColumn3.y + r2 * mColumn3.z )
		);
	}

	__host__ __device__ bool isFinite() const
	{
		return mColumn0.isFinite() && mColumn1.isFinite() && mColumn2.isFinite() && mColumn3.isFinite();
	}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

public:

	Vec4 mColumn0;
	Vec4 mColumn1;
	Vec4 mColumn2;
	Vec4 mColumn3;

}; // class Matrix44


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ Transform::Transform( const Matrix44& m)
{
	Vec3 column0  = Vec3(m.mColumn0.x, m.mColumn0.y, m.mColumn0.z);
	Vec3 column1  = Vec3(m.mColumn1.x, m.mColumn1.y, m.mColumn1.z);
	Vec3 column2  = Vec3(m.mColumn2.x, m.mColumn2.y, m.mColumn2.z);

	q = Quaternion( Matrix33( column0, column1, column2 ) );
	v = Vec3( m.mColumn3.x, m.mColumn3.y, m.mColumn3.z );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
