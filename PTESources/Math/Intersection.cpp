#include "PTESources/PCH.hpp"
#include "PTESources/Math/Intersection.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ void
Intersection::set( const Intersection& that )
{
	memcpy( this, &that, sizeof(Intersection) );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ const Vec3&
Intersection::calculatedPNandReturn() const
{
	Assert( mHitTriangle && hasOnePointContact() );

	int v0 = ( impactNorm.dot( mHitTriangle->trig.n0 ) >= 0 ) & 1;
	int nv0 = v0 ^ 1;

	int v1 = ( impactNorm.dot( mHitTriangle->trig.n1 ) >= 0 ) & 1;
	int nv1 = v1 ^ 1;

	int v2 = ( impactNorm.dot( mHitTriangle->trig.n2 ) >= 0 ) & 1;
	int nv2 = v2 ^ 1;

	Assert( v0 + nv0 == 1 && v1 + nv1 == 1 && v2 + nv2 == 1 );
	
	pointNormal += v0  * mHitTriangle->trig.n0 * Up0;
	pointNormal -= nv0 * mHitTriangle->trig.n0 * Up0;

	pointNormal += v1  * mHitTriangle->trig.n1 * Vp1;
	pointNormal -= nv1 * mHitTriangle->trig.n1 * Vp1;

	pointNormal += v2  * mHitTriangle->trig.n2 * Wp2;
	pointNormal -= nv2 * mHitTriangle->trig.n2 * Wp2;

	pointNormal.voidNormalize();
	normalExtraction = &Intersection::getCalculatedPN;
	return pointNormal;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ void 
Intersection::setBestIsec( float maxQuadDist, const Intersection& that )
{
	if ( !that.hasOnePointContact() )
		return;

	if ( !hasOnePointContact() )
	{
		set( that );
		return;
	}

 	if ( that.mQuadDistance <= maxQuadDist && mQuadDistance > that.mQuadDistance )
 		set( that );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ void
Intersection::intersect( Intersection& out, const Ray& ray, const Triangle& trig )
{
	//http://geomalgorithms.com/a06-_intersect-2.html

	Vec3		u, v, triangleNormal;	// triangle vectors
	Vec3		w0, w;					// ray vectors
	float		a, b;					// params to calc ray-plane intersect

	// get triangle edge vectors and plane normal
	u = trig.p1 - trig.p0;
	v = trig.p2 - trig.p0;
	
	out.mType = ContactType::disjoint;

	triangleNormal = u.cross( v );
	if ( triangleNormal.isZero() )      
	{
		out.mType = ContactType::degenerate;
		return;
	}

	ray.dir;
	w0 = ray.origin - trig.p0;
	a = - triangleNormal.dot( w0 );
	b = triangleNormal.dot( ray.dir );

	if ( fabs( b ) < 0 )  // ray is  parallel to triangle plane
	{   
// 		if ( a == 0 )
// 			out.mType = ContactType::samePlane;
// 		else
// 			out.mType = ContactType::disjoint;
		return;
	}

	// get intersect point of ray with triangle plane
	// for a segment, also test if (r > 1.0) => no intersect
	float r = a / b;
	if ( r <= 0 )	// ray goes away from triangle
	{
		return;
	}
	
	out.mContact = ray.origin + r * ray.dir; // intersect point of ray and plane

	// is I inside T?
	float    uu, uv, vv, wu, wv, D;

	uu = u.dot( u );
	uv = u.dot( v );
	vv = v.dot( v );
	w  = out.mContact - trig.p0;
	wu = w.dot( u );
	wv = w.dot( v );
	D  = uv * uv - uu * vv;

	// get and test parametric coords
	float s, t;
	s = ( uv * wv - vv * wu ) / D;
	if ( s < 0 || s > 1 )			// I is outside T
	{
		return;
	}

	t = ( uv * wu - uu * wv ) / D;
	if ( t < 0 || (s + t) > 1 )		// I is outside T
	{
		return;
	}


	if ( ray.dir.dot( trig.trueNormal ) >= 0 )
		out.impactNorm = - trig.trueNormal;
	else
		out.impactNorm =   trig.trueNormal;

	out.Up0 = 1 - s - t;
	out.Vp1 = s;
	out.Wp2 = t;

	out.mType = ContactType::onePoint;
	out.mQuadDistance = ( ray.origin - out.mContact ).magnitudeSquared();

} // Intersection::intersect


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ void
Intersection::intersect( Intersection& out, const Ray& ray, const BoundedTriangle& trig )
{
	float trash;
	if ( !intersect( ray, trig.box, trash ) )
	{
		out.mType = ContactType::disjoint;
		return;
	}

	intersect( out, ray, trig.trig );
	out.mHitTriangle = &trig;

} // Intersection::intersect


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ bool
Intersection::intersect( const Ray &r, const AABB& box, float& isecDist )
{	

	//http://jcgt.org/published/0002/02/02/paper.pdf
	float tmin = 0;
	float tmax = 9E+35f;
	float txmin, txmax, tymin, tymax, tzmin, tzmax;
	txmin = (box.bounds[  r.sign[0] ].x - r.origin.x ) * r.invDir.x;
	txmax = (box.bounds[1-r.sign[0] ].x - r.origin.x ) * r.invDir.x;
		
	tymin = (box.bounds[  r.sign[1] ].y - r.origin.y ) * r.invDir.y;
	tymax = (box.bounds[1-r.sign[1] ].y - r.origin.y ) * r.invDir.y;

	tzmin = (box.bounds[  r.sign[2] ].z - r.origin.z ) * r.invDir.z;
	tzmax = (box.bounds[1-r.sign[2] ].z - r.origin.z ) * r.invDir.z;
	tmin = maximal( tzmin, maximal( tymin, maximal( txmin, tmin ) ) );
	tmax = minimal( tzmax, minimal( tymax, minimal( txmax, tmax ) ) );
	tmax *= 1.00000024f;

	isecDist = tmin;
	if ( tmin <= tmax )
	{
		isecDist = tmin;
		return true;
	}
	else
	{
		isecDist = 9E+35f;
		return false;
	}


//  	float tmin = 0, tmax = 9E+30f;
//  	float lo = r.invDir.x*(box.min().x - r.origin.x);
//  	float hi = r.invDir.x*(box.max().x - r.origin.x);
//  
//  	tmin = minimal(lo, hi);
//  	tmax = maximal(lo, hi);
//  	
//  	float lo1 = r.invDir.y*(box.min().y - r.origin.y);
//  	float hi1 = r.invDir.y*(box.max().y - r.origin.y);
//  
//  	tmin = maximal(tmin, minimal(lo1, hi1));
//  	tmax = minimal(tmax, maximal(lo1, hi1));
//  	
//  	float lo2 = r.invDir.z*(box.min().z - r.origin.z);
//  	float hi2 = r.invDir.z*(box.max().z - r.origin.z);
//  
//  	tmin = maximal(tmin, minimal(lo2, hi2));
//  	tmax = minimal(tmax, maximal(lo2, hi2));
//  
//  	isecDist = tmin;
//  
//  	return (tmin <= tmax) && (tmax > 0);


// 	float tmin, tmax, tymin, tymax, tzmin, tzmax;
// 	tmin	= ( box.bounds[   r.sign[0] ].x - r.origin.x) * r.invDir.x;
// 	tmax	= ( box.bounds[ 1-r.sign[0] ].x - r.origin.x) * r.invDir.x;
// 	tymin	= ( box.bounds[   r.sign[1] ].y - r.origin.y) * r.invDir.y;
// 	tymax	= ( box.bounds[ 1-r.sign[1] ].y - r.origin.y) * r.invDir.y;
// 	
// 	if ( (tmin > tymax) || (tymin > tmax) )
// 		return false;
// 	
// 	if (tymin > tmin)
// 		tmin = tymin;
// 	
// 	if (tymax < tmax)
// 		tmax = tymax;
// 
// 	tzmin = ( box.bounds[   r.sign[2] ].z - r.origin.z) * r.invDir.z;
// 	tzmax = ( box.bounds[ 1-r.sign[2] ].z - r.origin.z) * r.invDir.z;
// 	
// 	if ( (tmin > tzmax) || (tzmin > tmax) )
// 		return false;
// 
// 	if (tzmin > tmin)
// 		tmin = tzmin;
// 	
// 	if (tzmax < tmax)
// 		tmax = tzmax;
// 
// 	//http://gamedev.stackexchange.com/questions/26958/unsure-how-to-decide-ray-intersection-interval
// 	// choose bounds
// 
// 	float t0 = 0.00001f;
// 	float t1 = 9E+20f;
// 
// 	isecDist = tmin;
// 	return tmin < t1 && tmax > t0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ bool
Intersection::intersect( const AABB& b1, const AABB& b2 )
{	
	//http://studiofreya.com/blog/3d-math-and-physics/simple-aabb-vs-aabb-collision-detection/s

	volatile bool check = ( fabs(b1.center.x - b2.center.x) > (b1.halfSize.x + b2.halfSize.x) );
			
	check = check ||	  ( fabs(b1.center.y - b2.center.y) > (b1.halfSize.y + b2.halfSize.y) );
	
	check = check ||	  ( fabs(b1.center.z - b2.center.z) > (b1.halfSize.z + b2.halfSize.z) );
		
	return ! check;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
