#ifndef __GLVIEWPORT_ROUTINES_HPP__
#define __GLVIEWPORT_ROUTINES_HPP__

#include "PTESources/GL/glew.h"
#include "PTESources/GL/glut.h"
#include "PTESources/glm-0.9.2.7/glm/glm.hpp"
#include "PTESources/glm-0.9.2.7/glm/gtc/matrix_transform.hpp"
#include "PTESources/Math/Utility.hpp"

#include "RenderSystem\RenderBuffer.hpp"
#include "PTESources/RenderSystem/Scene.hpp"
#include "PTESources/RenderSystem/Color.hpp"
#include "PTESources/RenderSystem/RenderBuffer.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

extern PTE::Scene * gScene;// = 0;
extern PTE::Camera * gCamera;// = 0;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{
namespace GL{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// scene globals
struct SceneSettings
{
public:

	SceneSettings();
	// Helper rotation function.  
	glm::mat3 Get3x3RotMatrix(const float degrees, const glm::vec3& up) const;

	void defaultPrefsLookAt();

public:

	float FOV;
	float near_clip;
	float far_clip;
	float restInterestDistance ;

	float zoom_step;
	float orbit_step;
	float pan_step;

	float lastFrameEvalTime;

	glm::vec3 cam_pos;
	glm::vec3 cam_interest;
	glm::vec3 cam_up;
	glm::vec3 cam_restPos;


	int winSize_h;
    int winSize_w;

	bool drawHomeGrid;
	bool drawAllAsWire;
	bool drawWireOnShaded;
	bool drawTextures;
	bool drawPathtraced;

	bool lightIsOmni;

	bool drawTrigs;
	bool drawEmptyAABBs;

	int maxDepthDraw;
	bool drawTree;
	//int motion_modificator_key; // alt by default

}; // struct SceneSettings

static SceneSettings gPrefs;


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// hook mouse and key states
struct UserInputState 
{
	int m_clickX;
	int m_clickY;

	glm::vec3 m_clickCameraPos;
	glm::vec3 m_clickCameraInterest;
	glm::vec3 m_clickCameraUp;

	int m_state;
	int m_button;
	bool m_isModKeyPressed;

}; // struct UserInputState
static UserInputState gInputState;


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// display base stuff
namespace DisplayPrimitives
{
	void DrawSphere ( GLfloat in_size );
	
	void DrawTeapot (  GLfloat in_size );
	
	void DrawHomeGrid ( GLfloat in_length );
		
	void DrawPoints ( GLfloat in_size );
		
	void DrawDebugTriangles ( PTE::Camera & in_camera );
	
	void DrawDebugBVH (  BVHNode* node, int depth );

} // namespace DisplayPrimitives


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


namespace Callbacks
{
	
	void ShowFPS ( float _dt );
	void SetLight ();
	void SetOrthographicProjection();
	void SetPerspectiveProjection();
	//void DisplayLabel ( float _val, std::string & _info, float xpos, float ypos );
	void PrintHelp ();
	void InitFrame ();

	// registration callbacks
	void DisplayScene(void);
	void SpecialKeys(int key, int x, int y);

	void MouseClickEvent ( int button, int state, int x, int y );
	void MouseDragEvent (  int x, int y );
	void RegularKeys (unsigned char key,int x,int y);
	void ReshapeWindow ( int in_w,int in_h);

} // namespace Callbacks


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Timer
{
public:

	Timer ()
	{
		reset();
	}

	inline float getElapsedTime () 
	{   
		double tmFinal = clockDouble() - m_tmInit;
		m_tmInit = clockDouble();
		return float( tmFinal / 1000.f ); 
	}

	inline void reset ()
	{
		m_tmInit = clock();
	}

private:

	inline static float clock()
	{
		return float( ::clock() );
	}

	inline static double clockDouble()
	{
		return ::clock();
	}

private:

	double m_tmInit;
};



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace GL{
} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif // __GLVIEWPORT_ROUTINES_HPP__